package run.iget.admin.quartz.service.impl;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import run.iget.admin.quartz.entity.ScheduleJobLogEntity;
import run.iget.admin.quartz.mapper.ScheduleJobLogMapper;
import run.iget.admin.quartz.req.ScheduleJobLogReq;
import run.iget.admin.quartz.service.ScheduleJobLogService;
import run.iget.framework.common.req.PageReq;

import java.util.Objects;

/**
 * 定时任务日志
 *
 * @author 阿沐 babamu@126.com
 */
@Service
@AllArgsConstructor
public class ScheduleJobLogServiceImpl extends ServiceImpl<ScheduleJobLogMapper, ScheduleJobLogEntity>
        implements ScheduleJobLogService {
    @Override
    public Page<ScheduleJobLogEntity> page(PageReq<ScheduleJobLogReq> query) {
        ScheduleJobLogReq queryBean = query.getQueryBean();
        QueryWrapper queryWrapper = QueryWrapper.create()
                .orderBy(ScheduleJobLogEntity::getId).desc();
        if (Objects.nonNull(queryBean)) {
            queryWrapper
                    .where(ScheduleJobLogEntity::getJobName).like(queryBean.getJobName())
                    .and(ScheduleJobLogEntity::getJobGroup).like(queryBean.getJobGroup())
                    .and(ScheduleJobLogEntity::getJobId).like(queryBean.getJobId());
        }
        return this.page(query.getPage(), queryWrapper);
    }

}

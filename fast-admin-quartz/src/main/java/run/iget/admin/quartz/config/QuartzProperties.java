package run.iget.admin.quartz.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import run.iget.admin.quartz.constant.QuartzConst;
import run.iget.framework.propertity.ModuleProperties;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 定时任务模块配置
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/24 23:46
 */
@Data
@Component
@ConfigurationProperties(prefix = QuartzConst.MODULE_NAME)
public class QuartzProperties extends ModuleProperties {

}

package run.iget.admin.quartz.req;

import lombok.Data;
import lombok.EqualsAndHashCode;
import run.iget.framework.common.req.DateTimeRangeReq;

/**
 * 定时任务查询
 *
 * @author 阿沐 babamu@126.com
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ScheduleJobReq extends DateTimeRangeReq {
    private String  jobName;

    private String  jobGroup;

    private Integer status;

}

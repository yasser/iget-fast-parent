package run.iget.admin.quartz.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import run.iget.admin.quartz.entity.ScheduleJobEntity;

/**
* 定时任务
*
* @author 阿沐 babamu@126.com
*/
@Mapper
public interface ScheduleJobMapper extends BaseMapper<ScheduleJobEntity> {

}

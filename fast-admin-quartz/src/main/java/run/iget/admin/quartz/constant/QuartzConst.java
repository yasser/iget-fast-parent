package run.iget.admin.quartz.constant;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 常量对象
 * ---------------类描述-----------------
 * @author 大周
 * @date 2023/1/23 23:33
 */
public interface QuartzConst {

    String MODULE_NAME             = "fast.quartz";
    String CONTROLLER_PACKAGE_NAME = "run.iget.system.quartz.controller";

}

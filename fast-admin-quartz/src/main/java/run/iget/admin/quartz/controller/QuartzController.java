package run.iget.admin.quartz.controller;

import com.mybatisflex.core.paginate.Page;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import run.iget.admin.quartz.constant.QuartzConst;
import run.iget.admin.quartz.entity.ScheduleJobEntity;
import run.iget.admin.quartz.entity.ScheduleJobLogEntity;
import run.iget.admin.quartz.req.ScheduleJobLogReq;
import run.iget.admin.quartz.req.ScheduleJobReq;
import run.iget.admin.quartz.service.ScheduleJobLogService;
import run.iget.admin.quartz.service.ScheduleJobService;
import run.iget.admin.quartz.utils.CronUtils;
import run.iget.framework.common.req.PageReq;
import run.iget.framework.common.util.ExceptionThrowUtils;
import run.iget.framework.propertity.ModuleProperties;
import run.iget.security.annotation.AuthCheck;

import java.util.List;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 定时任务管理控制器
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/9 20:45
 */
@AuthCheck
@RequiredArgsConstructor
@RestController
@RequestMapping("/sys/quartz")
@ConditionalOnProperty(prefix = QuartzConst.MODULE_NAME, value = ModuleProperties.CONDITIONAL_ON_PROPERTY_VALUE_NAME)
public final class QuartzController {

    private final ScheduleJobService scheduleJobService;
    private final ScheduleJobLogService scheduleJobLogService;

    /**
     * 新增或更新
     *
     * @param vo 请求数据
     */
    @PostMapping("/saveOrUpdate")
    public void saveOrUpdate(@RequestBody ScheduleJobEntity vo) {
        ExceptionThrowUtils.ofFalse(CronUtils.isValid(vo.getCronExpression()), "Cron表达式不正确");
        scheduleJobService.saveOrUpdate(vo);
    }

    /**
     * 删除
     *
     * @param ids
     */
    @PostMapping("/delete")
    public void delete(@RequestBody List<Long> ids) {
        scheduleJobService.delete(ids);
    }

    /**
     * 分页查询
     *
     * @param query
     * @return
     */
    @PostMapping("/list")
    public Page<ScheduleJobEntity> list(@RequestBody PageReq<ScheduleJobReq> query) {
        return scheduleJobService.list(query);
    }

    /**
     * 查询
     *
     * @param vo
     * @return
     */
    @PostMapping("/load")
    public ScheduleJobEntity load(@RequestBody ScheduleJobEntity vo) {
        return scheduleJobService.getById(vo.getId());
    }

    @PostMapping("/run")
    public void run(@RequestBody ScheduleJobEntity vo) {
        scheduleJobService.run(vo);
    }

    @PostMapping("/change/status")
    public void changeStatus(@RequestBody ScheduleJobEntity vo) {
        scheduleJobService.changeStatus(vo);
    }

    @PostMapping("/task/log")
    public Page<ScheduleJobLogEntity> page(@RequestBody PageReq<ScheduleJobLogReq> query) {
        return scheduleJobLogService.page(query);
    }

}

package run.iget.admin.quartz.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;

import run.iget.admin.quartz.entity.ScheduleJobLogEntity;
import run.iget.admin.quartz.req.ScheduleJobLogReq;
import run.iget.framework.common.req.PageReq;

/**
 * 定时任务日志
 *
 * @author 阿沐 babamu@126.com
 */
public interface ScheduleJobLogService extends IService<ScheduleJobLogEntity> {

    Page<ScheduleJobLogEntity> page(PageReq<ScheduleJobLogReq> query);

}

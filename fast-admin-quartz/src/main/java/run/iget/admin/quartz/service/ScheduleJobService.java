package run.iget.admin.quartz.service;

import java.util.List;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;

import run.iget.admin.quartz.entity.ScheduleJobEntity;
import run.iget.admin.quartz.req.ScheduleJobReq;
import run.iget.framework.common.req.PageReq;

/**
 * 定时任务
 *
 * @author 阿沐 babamu@126.com
 */
public interface ScheduleJobService extends IService<ScheduleJobEntity> {

    void delete(List<Long> idList);

    void run(ScheduleJobEntity vo);

    void changeStatus(ScheduleJobEntity vo);

    Page<ScheduleJobEntity> list(PageReq<ScheduleJobReq> query);

}

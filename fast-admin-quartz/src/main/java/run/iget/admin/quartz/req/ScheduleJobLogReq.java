package run.iget.admin.quartz.req;

import lombok.Data;
import lombok.EqualsAndHashCode;
import run.iget.framework.common.req.DateTimeRangeReq;

/**
* 定时任务日志查询
*
* @author 阿沐 babamu@126.com
*/
@Data
@EqualsAndHashCode(callSuper = false)
public class ScheduleJobLogReq extends DateTimeRangeReq {

    private Long   jobId;

    private String jobName;

    private String jobGroup;

}

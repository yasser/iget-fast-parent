package run.iget.admin.quartz.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.entity.BaseEntity;

/**
 * 定时任务日志
 *
 * @author 阿沐 babamu@126.com
 */
@Data
@Table("sys_schedule_job_log")
public class ScheduleJobLogEntity extends BaseEntity {

    /**
     * 任务id
     */
    private Long jobId;

    /**
     * 任务名称
     */
    private String jobName;

    /**
     * 任务组名
     */
    private String jobGroup;

    /**
     * spring bean名称
     */
    private String beanName;

    /**
     * 执行方法
     */
    private String method;

    /**
     * 参数
     */
    private String params;

    /**
     * 任务状态
     */
    private Integer status;

    /**
     * 异常信息
     */
    private String error;

    /**
     * 耗时(单位：毫秒)
     */
    private Long times;
}

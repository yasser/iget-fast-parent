package run.iget.security.service;

import run.iget.framework.common.util.ExceptionThrowUtils;
import run.iget.security.bean.SafeAuthUser;
import run.iget.security.req.LoginReq;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 登录接口
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2022/8/25 17:30
 */
public interface UserAuthService {

    /**
     * 处理账号+密码登录
     */
    default SafeAuthUser login(LoginReq loginReq) {
        ExceptionThrowUtils.of("登录接口未做实现");
        return null;
    }
}

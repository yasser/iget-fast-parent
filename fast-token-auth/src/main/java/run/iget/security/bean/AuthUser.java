package run.iget.security.bean;

import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 授权用户
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2022/8/25 15:06
 */
@Data
public class AuthUser extends TokenInfo {


    /**
     * 登录的终端，可以做统一账号，不同终端登录
     */
    private String terminal;

    /**
     * 用户昵称
     */
    private String username;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 所拥有的权限
     */
    private Set<String> permissions;

    /**
     * 菜单数据
     */
    private List menus;

    /**
     * 角色集合
     */
    private Set<Long> roles;

    /**
     * 部门集合
     */
    private Set<Long> departments;
}

package run.iget.security.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import run.iget.framework.common.util.PasswordUtils;

import java.util.Objects;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 授权用户
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2022/8/25 15:06
 */
@Data
public class SafeAuthUser extends AuthUser {

    /**
     * 二次认证的值
     * = PasswordUtils.md5(safe, password)
     */
    private String safeValue;

    /**
     * 二次认证的盐值
     */
    private String safeSalt;

    /**
     * 登录的终端系统名称
     */
    private String system;

    /**
     * 登录的ip地址
     */
    private String ip;

    /**
     * 登录的ip地址对应的省市区地址
     */
    private String ipAddress;


    @JsonIgnore
    public boolean isSafeValue(String password) {
        return Objects.equals(safeValue, PasswordUtils.md5(password, safeSalt));
    }

}

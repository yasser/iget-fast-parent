package run.iget.security.bean;

import lombok.Data;

import java.util.Date;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 权限校验的操作日志对象
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/8/8 14:27
 */
@Data
public class SafeAuthOptionLog {

    /**
     * 多账号体现下的分区标志
     */
    private String partition;

    /**
     * 用户唯一标识
     */
    private Long id;

    /**
     * 用户昵称
     */
    private String username;

    /**
     * 登录的终端，可以做统一账号，不同终端登录
     */
    private String terminal;

    /**
     * 登录的终端系统名称
     */
    private String system;

    /**
     * 登录的ip地址
     */
    private String ip;

    /**
     * 登录的ip地址对应的省市区地址
     */
    private String ipAddress;

    /**
     * 操作分类
     */
    private String category;

    /**
     * 页面
     */
    private String page;

    /**
     * 操作内容
     */
    private String content;

    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 耗时，单位毫秒
     */
    private Long castTime;
    /**
     * 请求类型：get、post
     */
    private String requestType;
    /**
     * 请求uri
     */
    private String requestUri;
    /**
     * 请求参数
     */
    private String requestParams;
    /**
     * 返回code
     */
    private String responseCode;
    /**
     * 返回消息
     */
    private String responseMsg;
    /**
     * 返回内容
     */
    private String responseData;
}

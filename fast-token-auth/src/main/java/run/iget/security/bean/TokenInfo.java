package run.iget.security.bean;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class TokenInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 多账号体现下的分区标志
     */
    private String partition;

    /**
     * 用户唯一标识
     */
    private Long id;

    /**
     * 用户token
     */
    private String token;

    /**
     * 过期时间
     */
    private Date expiresDate;

    @JsonIgnore
    public void genToken(int tokenExpireSeconds) {
        this.setToken(IdUtil.fastSimpleUUID() + SecureUtil.md5(partition));
        this.setExpiresDate(DateUtil.offsetSecond(new Date(), tokenExpireSeconds));
    }

    /**
     * 获取带分区标志的id
     *
     * @return
     */
    @JsonIgnore
    public String getPartitionOfId() {
        if (StrUtil.isBlank(partition)) {
            return String.valueOf(id);
        }
        return partition + "_" + this.id;
    }
}

package run.iget.security.constant;

public class SecurityConst {

    public final static String MODULE_NAME = "fast.auth";
    public final static String CONTROLLER_PACKAGE_NAME = "run.iget.framework.security.controller";

    public static String TOKEN_KEY = "x_token";
    public static String SAFE_KEY = "x_token";
    public static int TOKEN_EXPIRE_SECONDS = 1800;

}

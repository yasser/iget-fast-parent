package run.iget.security.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 权限校验注解，标记的的controller请求，必须是登录状态
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2022/8/24 10:05
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AuthCheck {

    /**
     * 是否启用
     * 配置优先级：方法 > 类
     */
    boolean enable() default true;

    /**
     * 是否校验uri路径，默认true
     * 判断登录账号的权限是否与uri一致
     */
    boolean checkUri() default true;

    /**
     * 是否需要二次认证校验
     * 如果开启，则会检查header中的密码与当前用户密码是否一致
     *
     * @return
     */
    boolean safeCheck() default false;

    /**
     * 必须有的角色标识集合
     *
     * @return
     */
    long[] requiredRoleIds() default {};

    /**
     * 分区标识，用于同一个应用里面，多套账号的时候进行区分
     *
     * @return
     */
    String partition() default "";

    /**
     * 操作分类
     *
     * @return
     */
    String optionCategory() default "";

    /**
     * 操作页面
     *
     * @return
     */
    String optionPage() default "";

    /**
     * 操作内容，为空时，填充uri
     *
     * @return
     */
    String optionContent() default "";
}

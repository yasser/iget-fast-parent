package run.iget.security.event;

import run.iget.framework.event.AppEvent;
import run.iget.security.bean.SafeAuthOptionLog;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 操作日志事件对象
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/8/8 14:25
 */
public class SafeAuthOptionLogEvent extends AppEvent {

    public SafeAuthOptionLogEvent(SafeAuthOptionLog source) {
        super(source);
    }
}

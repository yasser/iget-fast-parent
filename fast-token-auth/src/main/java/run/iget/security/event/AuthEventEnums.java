package run.iget.security.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import run.iget.framework.common.enums.BaseEnum;

@Getter
@AllArgsConstructor
public enum AuthEventEnums implements BaseEnum<String> {
    LOGIN("login", "登录"),
    LOGOUT("logout", "退出");

    private String code;
    private String desc;
}

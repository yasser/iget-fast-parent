package run.iget.security.event;

import run.iget.framework.event.AppEvent;
import run.iget.security.bean.AuthUser;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 退出事件对象
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/17 14:25
 */
public class LogoutEvent extends AppEvent {

    public LogoutEvent(AuthUser authUser) {
        super(authUser);
    }

}

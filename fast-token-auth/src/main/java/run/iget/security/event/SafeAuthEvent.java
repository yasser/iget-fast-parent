package run.iget.security.event;

import lombok.Getter;
import run.iget.framework.event.AppEvent;
import run.iget.security.bean.SafeAuthUser;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 登录事件对象
 * ---------------类描述-----------------
 * @author 大周
 * @date 2023/1/17 14:27
 */
@Getter
public class SafeAuthEvent extends AppEvent {

    private AuthEventEnums authEventEnum;

    public SafeAuthEvent(SafeAuthUser authUser, AuthEventEnums authEventEnum) {
        super(authUser);
        this.authEventEnum = authEventEnum;
    }

}

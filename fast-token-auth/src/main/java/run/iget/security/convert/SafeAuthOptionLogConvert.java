package run.iget.security.convert;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;
import run.iget.security.bean.SafeAuthOptionLog;
import run.iget.security.bean.SafeAuthUser;

import java.util.Date;

@Mapper(imports = {Date.class},
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, builder = @Builder(disableBuilder = true))
public interface SafeAuthOptionLogConvert {
    /**
     * mapstruct转换实例
     */
    SafeAuthOptionLogConvert I = Mappers.getMapper(SafeAuthOptionLogConvert.class);

    @Mapping(target = "startTime", expression = "java(new Date())")
    SafeAuthOptionLog to(SafeAuthUser source);

}

package run.iget.security.req;

import lombok.Data;
import run.iget.framework.captcha.req.CaptchaReq;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 登录请求对象
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2022/8/25 15:09
 */
@Data
public class LoginReq {

    /**
     * 登录的账号
     */
    private String     account;

    /**
     * 登录密码
     */
    private String     password;

    /**
     * 登录的终端，可以做统一账号，不同终端登录
     */
    private String     terminal;

    /**
     * 验证码信息
     */
    private CaptchaReq captcha;

}

package run.iget.security.config;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import run.iget.framework.propertity.ModuleProperties;
import run.iget.security.constant.SecurityConst;

import java.util.List;
import java.util.Objects;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 安全配置
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/24 00:16
 */
@Data
@Configuration
@ConfigurationProperties(prefix = SecurityConst.MODULE_NAME)
public class SecurityProperties extends ModuleProperties implements InitializingBean {

    /**
     * token创建和校验的key
     * 使用对称加密
     */
    private String tokenKey;

    /**
     * 二次确认header key
     */
    private String safeKey;

    /**
     * 有限期秒数：
     * 默认 30 * 60
     */
    private Integer tokenExpireSeconds;

    /**
     * 是否开启模拟账户id
     * 如果不为空，则表示没有登录的用户，默认设置为当前用户为此id
     */
    private Long mockAdminId;

    /**
     * mock的角色
     */
    private Long mockRoleId;

    /**
     * 是否禁用自动续期
     */
    private Boolean disableAutoRenewed;

    /**
     * 只需要登录校验
     */
    private List<String> needLogin;

    /**
     * 需要登录且有权限的校验
     */
    private List<String> needPermissions;

    @Override
    public void afterPropertiesSet() throws Exception {
        if (StrUtil.isNotBlank(tokenKey)) {
            SecurityConst.TOKEN_KEY = tokenKey;
        }
        if (StrUtil.isNotBlank(safeKey)) {
            SecurityConst.SAFE_KEY = safeKey;
        }
        if (Objects.nonNull(tokenExpireSeconds)) {
            SecurityConst.TOKEN_EXPIRE_SECONDS = tokenExpireSeconds;
        }
    }
}

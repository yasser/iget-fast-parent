package run.iget.admin.system.service.impl;

import org.springframework.stereotype.Service;

import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;

import run.iget.admin.system.entity.Role;
import run.iget.admin.system.entity.table.RoleTableDef;
import run.iget.admin.system.mapper.RoleMapper;
import run.iget.admin.system.service.RoleService;
import run.iget.framework.common.enums.YesOrNoEnum;

/**
 * <p>
 * 权限中心：角色表 服务实现类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {
    @Override
    public void add(Role entity) {
        entity.setId(null);
        // 设置pid
        this.save(entity);
    }

    @Override
    public void update(Role entity) {
        this.updateById(entity);
    }

    @Override
    public void delete(Long id) {
        QueryWrapper queryWrapper = QueryWrapper.create().where(RoleTableDef.ROLE.DELETABLE.eq(YesOrNoEnum.Y.getCode()))
                .and(RoleTableDef.ROLE.ID.eq(id));
        this.remove(queryWrapper);
    }
}

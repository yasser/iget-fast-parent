package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.entity.BaseEntity;

import java.util.Date;

/**
 * <p>
 * 管理员登录记录
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_admin_login_log")
public class AdminLoginLog extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 管理员id
     */
    private Integer administratorId;

    /**
     * 管理员昵称
     */
    private String administratorNickname;

    /**
     * 登录的终端：浏览器名称
     */
    private String terminal;

    /**
     * 登录的终端系统名称
     */
    private String system;

    /**
     * 登录的ip地址
     */
    private String ip;

    /**
     * 登录的ip地址对应的省市区地址
     */
    private String ipAddress;

    /**
     * 凭证过期时间
     */
    private Date tokenExpireDate;

}

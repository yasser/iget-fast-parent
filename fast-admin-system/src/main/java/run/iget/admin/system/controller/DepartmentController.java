package run.iget.admin.system.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import run.iget.admin.system.bean.RoleDepartmentDTO;
import run.iget.admin.system.entity.Department;
import run.iget.admin.system.service.DepartmentService;
import run.iget.security.annotation.AuthCheck;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 系统部门 前端控制器
 * </p>
 *
 * @author 大周|450153554@qq.com
 * @since 2021-02-21
 */
@AuthCheck
@RestController
@RequestMapping("/sys/department")
public class DepartmentController {

    @Resource
    private DepartmentService departmentService;

    /**
     * 新增
     *
     * @param entity
     */
    @PostMapping("/saveOrUpdate")
    public void saveOrUpdate(@RequestBody RoleDepartmentDTO entity) {
        departmentService.saveOrUpdate(entity);
    }

    /**
     * 查询全部
     *
     * @return
     */
    @PostMapping("/tree")
    public List<Department> tree(@RequestBody Department department) {
        return departmentService.tree(department);
    }

    /**
     * 删除部门数据
     * 如果部门下面有用户，则不可删除
     *
     * @param department 部门对象
     */
    public void delete(@RequestBody Department department) {
        //        departmentService.delete(department);
    }

    /**
     * 查询部门关的角色id集合
     *
     * @param department 部门对象
     * @return 关的角色id集合
     */
    @PostMapping("/queryRelationRoleIds")
    public List<Long> queryRelationRoleIds(@RequestBody Department department) {
        return departmentService.queryRelationRoleIds(department.getId());
    }
}

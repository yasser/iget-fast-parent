package run.iget.admin.system.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mybatisflex.core.paginate.Page;

import cn.hutool.core.collection.CollectionUtil;
import run.iget.admin.system.bean.AdministratorDTO;
import run.iget.admin.system.bean.req.AdministratorReq;
import run.iget.admin.system.entity.Administrator;
import run.iget.admin.system.service.AdministratorService;
import run.iget.framework.common.req.PageReq;
import run.iget.security.annotation.AuthCheck;

/**
 * <p>
 * 系统管理员表 前端控制器
 * </p>
 *
 * @author 大周|450153554@qq.com
 * @since 2021-02-21
 */
@AuthCheck
@RestController
@RequestMapping("/sys/administrator")
public class AdministratorController {

    @Resource
    private AdministratorService administratorService;

    /**
     * 新增或更新，同时更新关联的角色、部门数据
     *
     * @param administrator 请求数据
     */
    @PostMapping("/saveOrUpdate")
    public void saveOrUpdate(@RequestBody AdministratorDTO administrator) {
        administratorService.saveOrUpdate(administrator);
    }

    /**
     * 重置密码
     *
     * @param administrator
     */
    @PostMapping("/reset/password")
    public void resetPassword(@RequestBody AdministratorDTO administrator) {
        administratorService.updateById(administrator);
    }

    /**
     * 删除
     *
     * @param administrator
     */
    @PostMapping("/delete")
    public void delete(@RequestBody Administrator administrator) {
        administratorService.delete(administrator.getId());
    }

    /**
     * 分页查询
     *
     * @param pageReq
     * @return
     */
    @PostMapping("/list")
    public Page<Administrator> list(@RequestBody PageReq<AdministratorReq> pageReq) {
        Page<Administrator> administratorPage = administratorService.list(pageReq);
        if (CollectionUtil.isNotEmpty(administratorPage.getRecords())) {
            administratorPage.getRecords().forEach(item -> item.setSalt(null));
        }
        return administratorPage;
    }

    /**
     * 查询
     *
     * @param administrator
     * @return
     */
    @PostMapping("/load")
    public AdministratorDTO load(@RequestBody Administrator administrator) {
        return administratorService.load(administrator.getId());
    }

}

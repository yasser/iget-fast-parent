package run.iget.admin.system.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import run.iget.framework.common.enums.BaseEnum;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 事件类型
 * ---------------类描述-----------------
 * @author 大周
 * @date 2023/7/18 10:53
 */
@Getter
@AllArgsConstructor
public enum AdminSystemEventEnums implements BaseEnum<String> {
    ADMIN_ADD("admin_add", "新增管理员"),
    ADMIN_UPDATE("admin_update", "管理员数据变更"),
    ADMIN_DELETE("admin_delete", "管理员删除");

    private String code;
    private String desc;

}

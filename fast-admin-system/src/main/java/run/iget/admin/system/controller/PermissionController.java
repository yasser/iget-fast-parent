package run.iget.admin.system.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import run.iget.admin.system.entity.Permission;
import run.iget.admin.system.service.PermissionService;
import run.iget.security.annotation.AuthCheck;
import run.iget.security.util.LoginUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 授权中心：权限资源表 前端控制器
 * </p>
 *
 * @author 大周|450153554@qq.com
 * @since 2021-02-21
 */
@AuthCheck(requiredRoleIds = {1})
@RestController
@RequestMapping("/sys/permission")
public class PermissionController {

    @Resource
    private PermissionService permissionService;

    @PostMapping("/saveOrUpdate")
    public void saveOrUpdate(@RequestBody Permission entity) {
        if (Objects.isNull(entity.getId())) {
            permissionService.add(entity);
        } else {
            permissionService.updateById(entity);
        }
    }

    /**
     * 禁用
     *
     * @param entity
     */
    @PostMapping("/delete")
    public void delete(@RequestBody Permission entity) {
        // 只有超级管理员才可以删除
        permissionService.delete(entity.getId());
    }

    /**
     * 查询树形数据
     *
     * @return
     */
    @PostMapping("/tree")
    public List<Permission> tree() {
        return permissionService.listTreeByRoleIds(LoginUtils.getRoleIds());
    }

}

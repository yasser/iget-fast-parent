package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.entity.CreateTimeEntity;

/**
 * <p>
 * 角色-权限关联表
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_role_permission")
public class RolePermission extends CreateTimeEntity {

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 权限资源id
     */
    private Long permissionId;

}

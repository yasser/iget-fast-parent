package run.iget.admin.system.enums;

import lombok.Getter;

/**
 * Copyright (C), 2021，行动改变认知[iget.run]
 * 代码千万行，注释第一行
 * ---------------类描述--------------------
 * 菜单权限类型(0:目录;1:菜单;2:按钮,3页面中的tab)
 * ---------------类描述--------------------
 *
 * @author 大周|450513554@qq.com
 * @since 2021/2/8 10:20:00
 */
@Getter
public enum PermissionType {

    CATALOG(1, "目录"),
    MENU(2, "菜单"),
    BUTTON(3, "按钮"),
    TAB(4, "页面中的tab");

    /**
     * 关键值
     */
    private int    value;

    /**
     * 描述
     */
    private String description;

    PermissionType(int value, String description) {
        this.value = value;
        this.description = description;
    }

    /**
     * 根据父类型，获取子类型
     *
     * @param parentType 父类型
     * @return 默认 返回 CATALOG
     */
    public static PermissionType getSubType(Integer parentType) {
        if (CATALOG.getValue() == parentType) {
            return MENU;
        }
        if (MENU.getValue() == parentType) {
            return BUTTON;
        }
        return CATALOG;
    }
}

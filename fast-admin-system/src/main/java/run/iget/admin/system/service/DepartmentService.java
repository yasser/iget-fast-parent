package run.iget.admin.system.service;

import java.util.List;

import com.mybatisflex.core.service.IService;

import run.iget.admin.system.bean.RoleDepartmentDTO;
import run.iget.admin.system.entity.Department;

/**
 * <p>
 * 系统部门 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface DepartmentService extends IService<Department> {
    /**
     * 根节点id
     */
    final static Integer ROOT_NODE_ID = 0;

    /**
     * 查询树形结构部门数据
     *
     * @return
     */
    List<Department> tree(Department department);

    /**
     * 更新或新增，同事重置部门管理的角色
     *
     * @param roleDepartmentDTO 数据
     */
    void saveOrUpdate(RoleDepartmentDTO roleDepartmentDTO);

    /**
     * 查询部门关的角色id集合
     *
     * @param id 部门id
     * @return 关的角色id集合
     */
    List<Long> queryRelationRoleIds(Long id);

    /**
     * 删除部门数据
     * 如果部门下面有用户，则不可删除
     *
     * @param department 部门对象
     */
    void delete(Department department);
}

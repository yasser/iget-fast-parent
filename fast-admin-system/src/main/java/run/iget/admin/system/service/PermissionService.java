package run.iget.admin.system.service;

import com.mybatisflex.core.service.IService;
import run.iget.admin.system.entity.Permission;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 授权中心：权限资源表 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface PermissionService extends IService<Permission> {

    /**
     * 根据管理员id查询权限集合
     *
     * @param roleIds 角色id集合，已逗号分割
     * @return
     */
    List<Permission> listByRoleIds(String roleIds);

    /**
     * 根据管理员id查询权限集合
     *
     * @param roleIds 角色id集合
     * @return
     */
    List<Permission> listByRoleIds(List<Long> roleIds);


    /**
     * 根据管理员id查询权限集合
     *
     * @param roleIds 角色id集合
     * @return
     */
    List<Permission> listByRoleIds(Set<Long> roleIds);

    /**
     * 根据管理员id查询权限树形集合
     *
     * @param roleIds 角色id集合，已逗号分割
     * @return
     */
    List<Permission> listTreeByRoleIds(String roleIds);

    /**
     * 根据管理员id查询权限树形集合
     *
     * @param roleIds 角色id集合
     * @return
     */
    List<Permission> listTreeByRoleIds(Set<Long> roleIds);

    /**
     * 根据权限树
     *
     * @return List
     */
    List<Permission> listMenus();

    /**
     * 根据目录树
     *
     * @param permissionId permissionId
     * @return List
     */
    List<Permission> listMenus(String permissionId);

    /**
     * 将集合转为树形集合
     *
     * @param permissions -- 权限集合
     * @return
     */
    List<Permission> listToTree(List<Permission> permissions);

    /**
     * 构建菜单信息
     *
     * @param permissions
     * @return
     */
    List<Permission> buildMenus(List<Permission> permissions);

    /**
     * 新增
     *
     * @param entity
     */
    void add(Permission entity);

    /**
     * 删除
     *
     * @param id
     */
    void delete(Long id);

}

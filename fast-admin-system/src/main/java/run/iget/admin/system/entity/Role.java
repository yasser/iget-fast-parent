package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.entity.BaseTreeStatusEntity;
import run.iget.framework.common.enums.YesOrNoEnum;

/**
 * <p>
 * 权限中心：角色表
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_role")
public class Role extends BaseTreeStatusEntity {

    /**
     * 角色编码，唯一，方便查询下属所有子角色
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 简介
     */
    private String intro;

    /**
     * 是否可以被删除，N不可，Y可以
     *
     * @see YesOrNoEnum
     */
    private String deletable;

}

package run.iget.admin.system.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import run.iget.admin.system.entity.AdministratorDepartment;

/**
 * <p>
 * 管理员-部门关联表 Mapper 接口
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Mapper
public interface AdministratorDepartmentMapper extends BaseMapper<AdministratorDepartment> {

}

package run.iget.admin.system.service;

import java.util.List;

import com.mybatisflex.core.service.IService;

import run.iget.admin.system.entity.RoleDepartment;

/**
 * <p>
 * 角色部门 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface DepartmentRoleService extends IService<RoleDepartment> {
    /**
     * 部门管理多个角色
     *
     * @param departmentId
     * @param roleIds
     */
    void relationRole(Long departmentId, List<Long> roleIds);
}

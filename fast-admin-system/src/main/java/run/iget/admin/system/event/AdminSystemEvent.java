package run.iget.admin.system.event;

import lombok.Getter;
import run.iget.admin.system.entity.Administrator;
import run.iget.admin.system.enums.AdminSystemEventEnums;
import run.iget.framework.event.AppEvent;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 事件
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/7/18 11:00
 */
@Getter
public class AdminSystemEvent extends AppEvent {

    private AdminSystemEventEnums eventEnum;

    public AdminSystemEvent(Administrator source, AdminSystemEventEnums eventEnum) {
        super(source);
        this.eventEnum = eventEnum;
    }
}

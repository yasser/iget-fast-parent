package run.iget.admin.system.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import run.iget.admin.system.entity.RoleDepartment;

/**
 * <p>
 * 角色部门 Mapper 接口
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Mapper
public interface RoleDepartmentMapper extends BaseMapper<RoleDepartment> {

}

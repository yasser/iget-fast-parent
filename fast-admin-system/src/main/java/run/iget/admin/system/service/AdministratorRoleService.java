package run.iget.admin.system.service;

import java.util.List;

import com.mybatisflex.core.service.IService;

import run.iget.admin.system.entity.AdministratorRole;

/**
 * <p>
 * 管理员-角色关联表 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface AdministratorRoleService extends IService<AdministratorRole> {
    /**
     * 根据userId获取绑定的角色id
     *
     * @param adminId 管理员id
     * @return List
     */
    List<Long> getRoleIdsByUserId(Long adminId);
    //
    //    /**
    //     * 用户绑定角色
    //     *
    //     * @param vo vo
    //     */
    //    void addUserRoleInfo(UserRoleOperationReqVO vo);

    /**
     * 根据角色id获取绑定的人
     *
     * @param roleId roleId
     * @return List
     */
    List<Long> getUserIdsByRoleId(Long roleId);

    void relationRole(Long administratorId, List<Long> roleIds);
}

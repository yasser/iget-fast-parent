package run.iget.admin.system.service.impl;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import run.iget.admin.system.entity.AdminLoginLog;
import run.iget.admin.system.entity.table.AdminLoginLogTableDef;
import run.iget.admin.system.mapper.AdminLoginLogMapper;
import run.iget.admin.system.service.AdminLoginLogService;
import run.iget.framework.common.req.DateTimeRangeReq;
import run.iget.framework.common.req.PageReq;

import java.util.Objects;

/**
 * <p>
 * 管理员登录记录 服务实现类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Service
public class AdminLoginLogServiceImpl extends ServiceImpl<AdminLoginLogMapper, AdminLoginLog>
        implements AdminLoginLogService {

    @Override
    public Page<AdminLoginLog> list(PageReq<DateTimeRangeReq> pageReq) {
        QueryWrapper queryWrapper = QueryWrapper.create()
                .orderBy(AdminLoginLogTableDef.ADMIN_LOGIN_LOG.CREATE_TIME.desc());
        DateTimeRangeReq queryBean = pageReq.getQueryBean();
        if (Objects.nonNull(queryBean)) {
            queryWrapper.where(AdminLoginLogTableDef.ADMIN_LOGIN_LOG.CREATE_TIME.ge(queryBean.getStartTime()))
                    .and(AdminLoginLogTableDef.ADMIN_LOGIN_LOG.CREATE_TIME.le(queryBean.getEndTime()));
        }
        return this.page(pageReq.getPage(), queryWrapper);
    }
}

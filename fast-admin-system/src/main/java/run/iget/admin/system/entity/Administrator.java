package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.entity.BaseStatusEntity;
import run.iget.framework.desensitization.annotation.Desensitization;
import run.iget.framework.desensitization.enums.DesensitizationTypeEnum;
import run.iget.framework.desensitization.enums.EmptyHandler;

/**
 * <p>
 * 系统管理员表
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_administrator")
public class Administrator extends BaseStatusEntity {

    /**
     * 登录名称 as
     */
    private String account;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 登录密码 = md5(md5(原密码 + salt) + salt)
     */
    @Desensitization(type = DesensitizationTypeEnum.PASSWORD)
    private String password;

    /**
     * 密码盐值
     */
    @Desensitization(handler = EmptyHandler.class)
    private String salt;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 电子邮箱，重置密码使用
     */
    private String email;

    /**
     * 是否可以被删除，N不可，Y可以
     *
     * @see run.iget.framework.common.enums.YesOrNoEnum
     */
    private String deletable;
}

package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.entity.CreateTimeEntity;

/**
 * <p>
 * 角色部门
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_role_department")
public class RoleDepartment extends CreateTimeEntity {

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 部门id
     */
    private Long departmentId;

}

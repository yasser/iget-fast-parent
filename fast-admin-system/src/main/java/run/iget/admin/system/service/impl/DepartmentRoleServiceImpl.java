package run.iget.admin.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mybatisflex.spring.service.impl.ServiceImpl;

import cn.hutool.core.collection.CollectionUtil;
import run.iget.admin.system.entity.RoleDepartment;
import run.iget.admin.system.entity.table.RoleDepartmentTableDef;
import run.iget.admin.system.mapper.RoleDepartmentMapper;
import run.iget.admin.system.service.DepartmentRoleService;

/**
 * <p>
 * 角色部门 服务实现类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Service
public class DepartmentRoleServiceImpl extends ServiceImpl<RoleDepartmentMapper, RoleDepartment>
        implements DepartmentRoleService {
    @Override
    public void relationRole(Long departmentId, List<Long> roleIds) {
        // 删除之前管理的角色
        this.remove(RoleDepartmentTableDef.ROLE_DEPARTMENT.DEPARTMENT_ID.eq(departmentId));
        if (CollectionUtil.isNotEmpty(roleIds)) {
            List<RoleDepartment> departmentRoleList = new ArrayList<>(roleIds.size());
            RoleDepartment departmentRole = null;
            for (Long roleId : roleIds) {
                departmentRole = new RoleDepartment();
                departmentRole.setRoleId(roleId);
                departmentRole.setDepartmentId(departmentId);
                departmentRoleList.add(departmentRole);
            }
            this.saveBatch(departmentRoleList);
        }
    }
}

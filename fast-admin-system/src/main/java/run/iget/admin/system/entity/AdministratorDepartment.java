package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.entity.CreateTimeEntity;

/**
 * <p>
 * 管理员-部门关联表
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_administrator_department")
public class AdministratorDepartment extends CreateTimeEntity {

    /**
     * 管理员id
     */
    private Long administratorId;

    /**
     * 部门id
     */
    private Long departmentId;

}

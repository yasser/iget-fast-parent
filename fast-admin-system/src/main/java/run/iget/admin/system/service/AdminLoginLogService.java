package run.iget.admin.system.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;

import run.iget.admin.system.entity.AdminLoginLog;
import run.iget.framework.common.req.DateTimeRangeReq;
import run.iget.framework.common.req.PageReq;

/**
 * <p>
 * 管理员登录记录 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface AdminLoginLogService extends IService<AdminLoginLog> {

    /**
     * 分页查询
     */
    Page<AdminLoginLog> list(PageReq<DateTimeRangeReq> pageReq);

}

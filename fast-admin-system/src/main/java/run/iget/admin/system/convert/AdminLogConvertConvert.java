package run.iget.admin.system.convert;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import run.iget.admin.system.entity.AdminLoginLog;
import run.iget.security.bean.SafeAuthUser;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, builder = @Builder(disableBuilder = true))
public interface AdminLogConvertConvert {
    /**
     * mapstruct转换实例
     */
    AdminLogConvertConvert I = Mappers.getMapper(AdminLogConvertConvert.class);

    @Mapping(source = "expiresDate", target = "tokenExpireDate")
    @Mapping(source = "id", target = "administratorId")
    @Mapping(source = "username", target = "administratorNickname")
    AdminLoginLog to(SafeAuthUser source);
}

package run.iget.admin.system.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import run.iget.admin.system.entity.AdminLoginLog;

/**
 * <p>
 * 管理员登录记录 Mapper 接口
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Mapper
public interface AdminLoginLogMapper extends BaseMapper<AdminLoginLog> {

}

package run.iget.admin.system.bean;

import java.util.List;

import lombok.Data;
import run.iget.admin.system.entity.Department;

/**
 * 代码千万行，注释第一行；注释不规范，迭代两行泪
 * ----------------------------------------
 * 角色管理部门请求对象
 * ----------------------------------------
 * author: 大周
 * date: 2022/2/9 10:01
 **/
@Data
public class RoleDepartmentDTO extends Department {

    /**
     * 角色id集合
     **/
    private List<Long> roleIds;

}

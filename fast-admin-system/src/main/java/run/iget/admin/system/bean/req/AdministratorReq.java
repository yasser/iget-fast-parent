package run.iget.admin.system.bean.req;

import lombok.Data;
import run.iget.admin.system.entity.Administrator;

/**
 * Copyright (C), 2021，行动改变认知[iget.run]
 * 代码千万行，注释第一行
 * ---------------类描述--------------------
 * <p>
 * ---------------类描述--------------------
 *
 * @author 大周|450513554@qq.com
 * @since 2021/2/21 20:24:47
 */
@Data
public class AdministratorReq extends Administrator {

}

package run.iget.admin.system.bean;

import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * Copyright (C), 2021，行动改变认知[iget.run]
 * 代码千万行，注释第一行
 * ---------------类描述--------------------
 * 重置密码的请求对象
 * ---------------类描述--------------------
 *
 * @author 大周|450513554@qq.com
 * @since 2021/3/1 19:17:49
 */
@Data
public class ResetPasswordDTO {

    /**
     * 邮箱
     **/
    private String email;

    /**
     * 修改密码的token
     */
    @NotBlank(message = "不可为空")
    private String token;

    /**
     * 新的密码
     */
    @NotBlank(message = "不可为空")
    private String password;

}

package run.iget.admin.system.service;

import com.mybatisflex.core.service.IService;

import run.iget.admin.system.entity.Role;

/**
 * <p>
 * 权限中心：角色表 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface RoleService extends IService<Role> {
    /**
     * 新增
     * @param entity
     */
    void add(Role entity);

    /**
     * 修改
     * @param entity
     */
    void update(Role entity);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);
}

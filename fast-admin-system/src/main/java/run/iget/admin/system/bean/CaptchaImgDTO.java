package run.iget.admin.system.bean;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.Data;

/**
 * Copyright (C), 2021，行动改变认知[iget.run]
 * 代码千万行，注释第一行
 * ---------------类描述--------------------
 * 获取图形验证码的请求对象
 * ---------------类描述--------------------
 *
 * @author 大周|450513554@qq.com
 * @since 2021/2/20 17:34:00
 */
@Data
public class CaptchaImgDTO {
    /**
     * 图片宽度
     */
    @Min(value = 40, message = "最小40")
    @Max(value = 200, message = "最大200")
    private Integer with = 130;

    /**
     * 图片高度
     */
    @Min(value = 20, message = "最小20")
    @Max(value = 200, message = "最大200")
    private Integer high = 50;

}

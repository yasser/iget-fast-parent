package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.entity.BaseTreeStatusEntity;

/**
 * <p>
 * 系统部门
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_department")
public class Department extends BaseTreeStatusEntity {

    /**
     * 部门编号(规则：父级关系编码+自己的编码)
     */
    private String code;

    /**
     * 部门名称
     */
    private String name;

    /**
     * 管理者id
     */
    private Long managerId;
    
    /**
     * 管理者名称
     */
    private String managerName;

    /**
     * 孩子数量，每次新增子节点会更新+1
     **/
    private Integer childrenNum;

    /**
     * 简介
     **/
    private String intro;

    /**
     * 是否可以被删除，N不可，Y可以
     */
    private String deletable;

}

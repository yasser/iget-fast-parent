package run.iget.admin.system.constant;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 模块常量
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/7/18 10:13
 */
public interface AdminSystemConst {
    String MODULE_NAME             = "fast.admin.system";
    String CONTROLLER_PACKAGE_NAME = "run.iget.admin.system";

    /**
     * 超级管理员角色id
     */
    Long   ADMIN_ROLE_ID           = 1L;
    /**
     * 超级管理员id
     */
    Long   ADMIN_ID                = 1L;

    /**
     * 默认根节点id
     */
    Long   ROOT_NODE_ID            = 0L;

}

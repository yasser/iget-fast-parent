package run.iget.admin.system.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mybatisflex.core.query.QueryWrapper;

import run.iget.admin.system.entity.DataDictionary;
import run.iget.admin.system.service.DataDictionaryService;
import run.iget.security.annotation.AuthCheck;

/**
 * <p>
 * 系统数据字典表 前端控制器
 * </p>
 *
 * @author 大周|450153554@qq.com
 * @since 2021-02-21
 */
@AuthCheck
@RestController
@RequestMapping("/sys/dictionary")
public class DataDictionaryController {

    @Autowired
    private DataDictionaryService dataDictionaryService;

    /**
     * 查询列表
     *
     * @param dataDictionary
     * @return
     */
    @PostMapping("/list")
    public List<DataDictionary> list(@RequestBody DataDictionary dataDictionary) {
        return dataDictionaryService.list(QueryWrapper.create(dataDictionary));
    }

    /**
     * 查询列表
     *
     * @param dataDictionary
     * @return
     */
    @PostMapping("/tree")
    public List<DataDictionary> tree(@RequestBody DataDictionary dataDictionary) {
        return dataDictionaryService.tree(dataDictionary);
    }

    /**
     * 新增或修改
     *
     * @param dataDictionary
     */
    @PostMapping("/saveOrUpdate")
    public void saveOrUpdate(@RequestBody DataDictionary dataDictionary) {
        dataDictionaryService.saveOrUpdate(dataDictionary);
    }

}

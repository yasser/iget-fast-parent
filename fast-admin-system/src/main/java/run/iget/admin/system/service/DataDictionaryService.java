package run.iget.admin.system.service;

import java.util.List;

import com.mybatisflex.core.service.IService;

import run.iget.admin.system.entity.DataDictionary;

/**
 * <p>
 * 系统数据字典表 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface DataDictionaryService extends IService<DataDictionary> {

    List<DataDictionary> tree(DataDictionary dataDictionary);

}

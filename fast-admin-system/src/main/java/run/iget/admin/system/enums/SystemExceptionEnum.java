package run.iget.admin.system.enums;

import lombok.Getter;
import run.iget.framework.common.enums.BaseResultEnum;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * <p>
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2021/7/16 23:11:02
 */
@Getter
public enum SystemExceptionEnum implements BaseResultEnum {

    /**
     * 业务-登录
     */
    VERIFICATION_CODE_ERROR("1000", "验证码错误"),
    ACCOUNT_OR_PASSWORD_ERROR("1001", "账号或密码错误"),
    ACCOUNT_INVALIDATION_ERROR("1002", "账号已禁止登录"),
    ACCOUNT_NO_PERMISSION("1003", "账号没有关联任何权限"),
    CAPTCHA_INPUT_ERROR("1004", "验证码错误"),
    /**
     * 重制密码
     */
    LINK_IS_INVALID("1004", "验证码错误"),

    /**
     * 权限操作
     */
    PARENT_NODE_DOES_NOT_EXIST("1100", "父节点不存在"),
    NOT_HAS_CHILD_NODE("1101", "当前节点不允许有子节点"),

    ACCOUNT_EXIST("1102", "账号已存在，请重新输入");

    /**
     * 模块
     */
    private String module = "system";

    /**
     * 返回码
     */
    private String code;

    /**
     * 返回消息
     */
    private String desc;

    SystemExceptionEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}

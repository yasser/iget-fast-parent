package run.iget.admin.system.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.mybatisflex.core.BaseMapper;

import run.iget.admin.system.entity.Role;

/**
 * <p>
 * 权限中心：角色表 Mapper 接口
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

}

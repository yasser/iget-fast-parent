package run.iget.admin.system.service;

import java.util.List;

import com.mybatisflex.core.service.IService;

import run.iget.admin.system.entity.AdministratorDepartment;

/**
 * <p>
 * 管理员-部门关联表 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface AdministratorDepartmentService extends IService<AdministratorDepartment> {

    /**
     * 管理员对于部门
     *
     * @param administratorId
     * @param departmentIds
     */
    void relationDepartment(Long administratorId, List<Long> departmentIds);

    /**
     * administratorDepartment == null 则查询全部
     *
     * @param administratorDepartment
     * @return
     */
    List<AdministratorDepartment> query(AdministratorDepartment administratorDepartment);

    /**
     * 根据用户id查询关联的部门id集合
     *
     * @param administratorId 管理员id
     * @return 联的部门id集合
     */
    List<Long> getDepartmentIdsByUserId(Long administratorId);
}

package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.entity.CreateTimeEntity;

/**
 * <p>
 * 管理员-角色关联表
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_administrator_role")
public class AdministratorRole extends CreateTimeEntity {

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 管理员id
     */
    private Long administratorId;

}

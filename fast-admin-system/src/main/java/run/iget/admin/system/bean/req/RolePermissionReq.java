package run.iget.admin.system.bean.req;

import lombok.Data;

import java.util.List;

@Data
public class RolePermissionReq {

    private Long roleId;
    private List<Long> permissionIds;

}

package run.iget.admin.system.config;

import cn.hutool.crypto.SecureUtil;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import run.iget.admin.system.constant.AdminSystemConst;
import run.iget.framework.propertity.ModuleProperties;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * WebSocket配置信息
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/24 20:54
 */
@Data
@Component
@ConfigurationProperties(prefix = AdminSystemConst.MODULE_NAME)
public class AdminSystemProperties extends ModuleProperties {

    /**
     * 站点域名
     **/
    private String domain;

    /**
     * 初始化密码的页面地址
     **/
    private String initPasswordViewPath;

    /**
     * token过期时间，单位分钟
     */
    private Integer initPasswordTokenExpireTime = 20;

    /**
     * 站点的加密秘钥
     */
    private String secret;

    /**
     * 是否处理管理员登录，
     * 如果true，需要创建t_admin_login_log表
     */
    private Boolean handleAdminLoginLog;

    /**
     * 获取初始化密码的连接
     *
     * @param token token
     * @return 携带token的连接
     */
    public String getInitPasswordUrl(String token) {
        return domain + initPasswordViewPath + "?token=" + token;
    }

    /**
     * 信息加密
     *
     * @param content 原文
     * @return 密文
     */
    public String encodeBySecret(String content) {
        return SecureUtil.des(secret.getBytes()).encryptBase64(content);
    }

    /**
     * 信息解密
     *
     * @param content 密文
     * @return 原文
     */
    public String decodeBySecret(String content) {
        return SecureUtil.des(secret.getBytes()).decryptStr(content);
    }

}

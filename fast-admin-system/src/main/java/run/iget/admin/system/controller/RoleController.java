package run.iget.admin.system.controller;

import cn.hutool.core.util.RandomUtil;
import com.mybatisflex.core.query.QueryWrapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import run.iget.admin.system.bean.RoleDTO;
import run.iget.admin.system.bean.req.RolePermissionReq;
import run.iget.admin.system.constant.AdminSystemConst;
import run.iget.admin.system.entity.Role;
import run.iget.admin.system.service.RolePermissionService;
import run.iget.admin.system.service.RoleService;
import run.iget.framework.common.enums.YesOrNoEnum;
import run.iget.security.annotation.AuthCheck;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * <p>
 * 权限中心：角色表 前端控制器
 * </p>
 *
 * @author 大周|450153554@qq.com
 * @since 2021-02-21
 */
@AuthCheck(requiredRoleIds = {1})
@RestController
@RequestMapping("/sys/role")
public class RoleController {

    @Resource
    private RoleService roleService;

    @Resource
    private RolePermissionService rolePermissionService;

    /**
     * 新增
     *
     * @param entity
     */
    @PostMapping("/add")
    @Transactional
    public void add(@RequestBody RoleDTO entity) {
        entity.setPid(AdminSystemConst.ADMIN_ROLE_ID);
        entity.setCode(AdminSystemConst.ADMIN_ROLE_ID + RandomUtil.randomNumbers(4));
        entity.setDeletable(YesOrNoEnum.Y.getCode());
        roleService.add(entity);
        rolePermissionService.relationPermissions(entity.getId(), entity.getPermissionIds());
    }

    /**
     * 修改
     *
     * @param entity
     */
    @PostMapping("/update")
    public void update(@RequestBody RoleDTO entity) {
        roleService.update(entity);
        rolePermissionService.relationPermissions(entity.getId(), entity.getPermissionIds());
    }

    @PostMapping("/saveOrUpdate")
    public void saveOrUpdate(@RequestBody RoleDTO entity) {
        if (Objects.isNull(entity.getId())) {
            add(entity);
        } else {
            update(entity);
        }
    }

    /**
     * 禁用
     *
     * @param entity
     */
    @PostMapping("/delete")
    public void delete(@RequestBody Role entity) {
        roleService.delete(entity.getId());
    }

    /**
     * 查询全部
     *
     * @return
     */
    @PostMapping("/list")
    public List<Role> list(@RequestBody Role role) {
        return roleService.list(QueryWrapper.create(role));
    }

    /**
     * 角色关联权限
     *
     * @param req
     */
    @PostMapping("/relation/permission")
    public void relationPermission(@RequestBody RolePermissionReq req) {
        rolePermissionService.relationPermissions(req.getRoleId(), req.getPermissionIds());
    }

    /**
     * 查询角色下面的权限id集合
     *
     * @param entity
     * @return
     */
    @PostMapping("/list/permissionIds")
    public List<Long> listPermissionIds(@RequestBody Role entity) {
        return rolePermissionService.listPermissionIds(entity.getId());
    }

}

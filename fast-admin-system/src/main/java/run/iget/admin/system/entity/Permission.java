package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.admin.system.enums.PermissionType;
import run.iget.framework.common.entity.BaseTreeStatusEntity;

/**
 * <p>
 * 授权中心：权限资源表
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_permission")
public class Permission extends BaseTreeStatusEntity {

    /**
     * 名称
     */
    private String name;

    /**
     * 图标
     */
    private String icon;

    /**
     * 资源路径
     */
    private String uri;

    /**
     * 类型：0目录，1菜单，2页面按钮
     *
     * @see PermissionType
     */
    private Integer type;

    /**
     * 排序字段，从小到大
     */
    private Integer sort;

    /**
     * 对于前端页面模块路径
     */
    private String viewPath;

    /**
     * 是否可以被删除，0不可，1可以
     */
    @Column(ignore = true)
    private String deletable;

}

package run.iget.admin.system.bean;

import java.util.List;

import lombok.Data;
import run.iget.admin.system.entity.Role;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * <p>
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2021/7/24 18:08:14
 */
@Data
public class RoleDTO extends Role {

    /**
     * 权限id集合
     */
    private List<Long> permissionIds;

}

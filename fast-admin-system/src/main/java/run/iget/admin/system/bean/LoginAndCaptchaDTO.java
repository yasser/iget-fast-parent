package run.iget.admin.system.bean;

import javax.validation.constraints.NotBlank;

import lombok.Data;

/**
 * Copyright (C), 2021，行动改变认知[iget.run]
 * 代码千万行，注释第一行
 * ---------------类描述--------------------
 * 登录请求对象
 * ---------------类描述--------------------
 *
 * @author 大周|450513554@qq.com
 * @since 2021/2/20 18:40:52
 */
@Data
public class LoginAndCaptchaDTO extends LoginDTO {
    /**
     * 验证码key
     */
    @NotBlank(message = "不可为空")
    private String captchaKey;

    /**
     * 验证码value
     */
    @NotBlank(message = "不可为空")
    private String captchaCode;
}

package run.iget.admin.system.convert;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.factory.Mappers;

import run.iget.admin.system.bean.AdministratorDTO;
import run.iget.admin.system.entity.Administrator;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, builder = @Builder(disableBuilder = true))
public interface AdministratorConvert {
    /**
     * mapstruct转换实例
     */
    AdministratorConvert I = Mappers.getMapper(AdministratorConvert.class);

    AdministratorDTO to(Administrator source);

    Administrator to(AdministratorDTO source);
}

package run.iget.admin.system.config;

import javax.annotation.Resource;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import run.iget.admin.system.constant.AdminSystemConst;
import run.iget.framework.propertity.ApiPathPrefixUtils;
import run.iget.framework.propertity.ModuleProperties;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * controller请求前缀配置
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/18 12:13
 */
@Configuration
@ConditionalOnProperty(prefix = AdminSystemConst.MODULE_NAME, value = ModuleProperties.CONDITIONAL_ON_PROPERTY_VALUE_NAME)
public class AdminSystemWebMvcConfigurer implements WebMvcConfigurer {

    @Resource
    private AdminSystemProperties properties;

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        // 为指定包下的controller添加前缀
        ApiPathPrefixUtils.addApiPathPrefix(properties, configurer, AdminSystemConst.CONTROLLER_PACKAGE_NAME);
    }
}

package run.iget.admin.system.service;

import java.util.List;

import com.mybatisflex.core.service.IService;

import run.iget.admin.system.entity.RolePermission;

/**
 * <p>
 * 角色-权限关联表 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface RolePermissionService extends IService<RolePermission> {

    /**
     * 角色关联权限
     *
     * @param roleId
     * @param permissionIds
     */
    void relationPermissions(Long roleId, List<Long> permissionIds);

    /**
     * 根据角色di查询权限id集合
     *
     * @param roleId
     * @return
     */
    List<Long> listPermissionIds(Long roleId);

    /**
     * 默认角色关联新的权限
     *
     * @param permissionId
     */
    void relationNewPermission(Long permissionId);
}

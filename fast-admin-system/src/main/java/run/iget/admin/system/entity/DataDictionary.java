package run.iget.admin.system.entity;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.annotation.Table;
import lombok.Data;
import run.iget.framework.common.enums.YesOrNoEnum;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 系统数据字典表
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
@Data
@Table("sys_data_dictionary")
public class DataDictionary {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键
     */
    @Id(keyType = KeyType.Auto)
    private Long id;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 分组编码
     */
    private String groupCode;

    /**
     * 编码，唯一
     */
    private String code;

    /**
     * 编码对应的值
     */
    private String value;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 状态，0未启用，1已启用
     */
    private Integer status;

    /**
     * 是否可以被删除
     *
     * @see YesOrNoEnum
     */
    private String deletable;

    /**
     * 子节点
     */
    @Column(ignore = true)
    private List children;
}

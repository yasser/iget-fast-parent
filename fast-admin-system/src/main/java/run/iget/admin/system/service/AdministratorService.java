package run.iget.admin.system.service;

import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.service.IService;

import run.iget.admin.system.bean.AdministratorDTO;
import run.iget.admin.system.bean.ResetPasswordDTO;
import run.iget.admin.system.bean.req.AdministratorReq;
import run.iget.admin.system.entity.Administrator;
import run.iget.framework.common.req.PageReq;

/**
 * <p>
 * 系统管理员表 服务类
 * </p>
 *
 * @author 大周
 * @since 2022-02-07
 */
public interface AdministratorService extends IService<Administrator> {

    /**
     * 新增或更新
     *
     * @param administrator
     */
    void saveOrUpdate(AdministratorDTO administrator);

    /**
     * 逻辑删除
     *
     * @param adminId
     */
    void delete(Long adminId);

    /**
     * 分页查询
     *
     * @param pageReq
     * @return
     */
    Page<Administrator> list(PageReq<AdministratorReq> pageReq);

    void sendInitPasswordEmail(Administrator administrator);

    void resetPassword(ResetPasswordDTO resetPasswordDTO);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    AdministratorDTO load(Long id);
}

# iget-fast-parent

#### 介绍

快速实现 springboot 的基础包
开发一些自己项目中常用的功能合集，方便 springboot 单体应用开发时快速集成

-   修改版本命令：

```mvn
    mvn versions:set -DnewVersion=1.2.0-SNAPSHOT
```

## 更新日志

-   V0.02

```
【A】新增fast-framework模块
【A】新增fast-web-socket模块
```

-   V0.01

```
init 初始化项目
```

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技

1.  使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

package run.iget.framework.cache.impl;

import cn.hutool.core.util.StrUtil;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.Expiry;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.index.qual.NonNegative;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Component;
import run.iget.framework.cache.CacheService;
import run.iget.framework.cache.CacheUtils;

import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@ConditionalOnExpression("'${spring.redis.host:}'.equals('')")
public class DefaultCacheService implements CacheService, InitializingBean {

    private static final Cache<String, Object> CAPTCHA_TIMED_CACHE = Caffeine.newBuilder()
            .expireAfter(new Expiry<String, Object>() {
                @Override
                public long expireAfterCreate(@NonNull String key, @NonNull Object value, long currentTime) {
                    return 0;
                }

                @Override
                public long expireAfterUpdate(@NonNull String key, @NonNull Object value, long currentTime,
                                              @NonNegative long currentDuration) {
                    return 0;
                }

                @Override
                public long expireAfterRead(@NonNull String key, @NonNull Object value, long currentTime,
                                            @NonNegative long currentDuration) {
                    return 0;
                }
            }).initialCapacity(100).maximumSize(1000).build();

    @Override
    public void set(String key, Object value) {
        CAPTCHA_TIMED_CACHE.put(key, value);
    }

    @Override
    public void set(String key, Object value, int timeoutSeconds) {
        CAPTCHA_TIMED_CACHE.policy().expireVariably().ifPresent(e -> {
            e.put(key, value, timeoutSeconds, TimeUnit.SECONDS);
        });
    }

    @Override
    public Object get(String key) {
        if (StrUtil.isBlank(key)) {
            return null;
        }
        return CAPTCHA_TIMED_CACHE.getIfPresent(key);
    }

    @Override
    public void remove(String key) {
        if (StrUtil.isNotBlank(key)) {
            CAPTCHA_TIMED_CACHE.invalidate(key);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("Caffeine初始化完成");
        CacheUtils.setCacheService(this, false);
    }
}

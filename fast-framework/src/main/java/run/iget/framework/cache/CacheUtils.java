package run.iget.framework.cache;

import java.util.Objects;

public class CacheUtils {

    private static CacheService CACHE_SERVICE;

    public static void set(String key, Object value) {
        CACHE_SERVICE.set(key, value);
    }

    public static void set(String key, Object value, int timeoutSeconds) {
        CACHE_SERVICE.set(key, value, timeoutSeconds);
    }

    public static Object get(String key) {
        return CACHE_SERVICE.get(key);
    }

    public static void remove(String key) {
        CACHE_SERVICE.remove(key);
    }

    public static void setCacheService(CacheService cacheService, boolean isOverwrite) {
        if (isOverwrite || Objects.isNull(CACHE_SERVICE)) {
            CACHE_SERVICE = cacheService;
        }
    }
}

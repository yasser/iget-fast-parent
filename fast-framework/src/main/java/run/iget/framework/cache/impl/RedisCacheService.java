package run.iget.framework.cache.impl;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import run.iget.framework.cache.CacheService;
import run.iget.framework.cache.CacheUtils;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@ConditionalOnExpression("!'${spring.redis.host:}'.equals('')")
public class RedisCacheService implements CacheService, InitializingBean {

    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public void set(String key, Object value) {
        redisTemplate.opsForValue().set(key, value);
    }

    @Override
    public void set(String key, Object value, int timeoutSeconds) {
        redisTemplate.opsForValue().set(key, value, timeoutSeconds, TimeUnit.SECONDS);
    }

    @Override
    public Object get(String key) {
        if (StrUtil.isBlank(key)) {
            return null;
        }
        return redisTemplate.opsForValue().get(key);
    }

    @Override
    public void remove(String key) {
        if (StrUtil.isNotBlank(key)) {
            redisTemplate.delete(key);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.info("redis缓存完成");
        CacheUtils.setCacheService(this, true);
    }
}

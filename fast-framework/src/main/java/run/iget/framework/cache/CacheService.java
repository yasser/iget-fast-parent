package run.iget.framework.cache;

public interface CacheService {

    void set(String key, Object value);

    void set(String key, Object value, int timeoutSeconds);

    Object get(String key);

    void remove(String key);
}

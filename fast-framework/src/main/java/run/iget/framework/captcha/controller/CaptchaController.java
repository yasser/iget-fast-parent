package run.iget.framework.captcha.controller;

import javax.annotation.Resource;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import run.iget.framework.captcha.constant.CaptchaConst;
import run.iget.framework.captcha.req.CaptchaReq;
import run.iget.framework.captcha.resp.CaptchaResp;
import run.iget.framework.captcha.service.CaptchaService;
import run.iget.framework.propertity.ModuleProperties;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 验证码控制器
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/24 20:47
 */
@RestController
@RequestMapping("/common/captcha")
@ConditionalOnProperty(prefix = CaptchaConst.MODULE_NAME, value = ModuleProperties.CONDITIONAL_ON_PROPERTY_VALUE_NAME)
public class CaptchaController {

    @Resource
    private CaptchaService captchaService;

    @PostMapping("")
    public CaptchaResp captcha(Integer timeOutSe) {
        return captchaService.generate(timeOutSe);
    }

    @PostMapping("/verify")
    public void verify(@RequestBody CaptchaReq req) {
        captchaService.verify(req);
    }

}

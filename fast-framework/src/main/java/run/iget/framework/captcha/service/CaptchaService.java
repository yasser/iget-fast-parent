package run.iget.framework.captcha.service;

import run.iget.framework.captcha.req.CaptchaReq;
import run.iget.framework.captcha.resp.CaptchaResp;

public interface CaptchaService {

    /**
     * 生成验证码信息
     * @param  timeoutSeconds 过期时间，为空，则默认10分钟
     * @return
     */
    CaptchaResp generate(Integer timeoutSeconds);

    /**
     * 对验证码进行校验，校验不通过则抛出异常
     * @param captchaReq
     */
    void verify(CaptchaReq captchaReq);
}

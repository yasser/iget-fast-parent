package run.iget.framework.captcha;

import lombok.Data;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 验证码基类
 * ---------------类描述-----------------
 * @author 大周
 * @since 2022/8/25 09:51
 */
@Data
public class CaptchaDTO {

    /**
     * 验证码对应的key
     */
    private String key;
}

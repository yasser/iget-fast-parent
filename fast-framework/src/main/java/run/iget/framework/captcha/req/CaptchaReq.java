package run.iget.framework.captcha.req;

import lombok.Data;
import run.iget.framework.captcha.CaptchaDTO;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 前端输入的验证码对象
 * ---------------类描述-----------------
 * @author 大周
 * @since 2022/8/25 09:51
 */
@Data
public class CaptchaReq extends CaptchaDTO {

    /**
     * 前端输入的验证码值
     */
    private String captcha;

}

package run.iget.framework.captcha.resp;

import lombok.Data;
import run.iget.framework.captcha.CaptchaDTO;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 输出给前端的验证码对象
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2022/8/25 09:51
 */
@Data
public class CaptchaResp extends CaptchaDTO {

    /**
     * 验证码图片的base64字符串
     */
    private String image;

}

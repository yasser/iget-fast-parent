package run.iget.framework.mail.service;

import java.io.File;
import java.util.Map;

/**
 * 代码千万行，注释第一行；注释不规范，迭代两行泪
 * ----------------------------------------
 * 邮件发送接口，默认异步实现
 * ----------------------------------------
 * author: 大周
 * date: 2022/2/8 14:37
 **/
public interface MailService {

    /**
     * 发送普通文本邮件
     *
     * @param to      收件人
     * @param subject 主题
     * @param content 内容
     */
    void sendSimpleMail(String to, String subject, String content);

    /**
     * 发送HTML邮件
     *
     * @param to      收件人
     * @param subject 主题
     * @param content 内容（可以包含<html>等标签）
     */
    void sendHtmlMail(String to, String subject, String content);

    /**
     * 发送带附件的邮件
     *
     * @param to              收件人
     * @param subject         主题
     * @param content         内容
     * @param attachmentFiles 附件集合，key为附件名称，value为附件对象
     */
    void sendHtmlMailOfAttachments(String to, String subject, String content, Map<String, File> attachmentFiles);

    /**
     * 发送带图片的邮件
     *
     * @param to         收件人
     * @param subject    主题
     * @param content    文本
     * @param imageFiles 图片集合，可以是 图片ID，用于在<img>标签中使用，从而显示图片，value是图片
     */
    void sendHtmlMailOfImages(String to, String subject, String content, Map<String, File> imageFiles);

    /**
     * 发送带图片的邮件
     *
     * @param to              收件人
     * @param subject         主题
     * @param content         文本
     * @param imageFiles      图片集合，可以是 图片ID，用于在<img>标签中使用，从而显示图片，value是图片
     * @param attachmentFiles 附件集合，key为附件名称，value为附件对象
     */
    void sendHtmlMail(String to, String subject, String content, Map<String, File> imageFiles, Map<String, File> attachmentFiles);

    /**
     * 发生重置密码邮件
     *
     * @param to  发生给谁
     * @param url 初始化密码的页面地址
     */
    void sendInitPasswordEmail(String to, String url);
}

package run.iget.framework.propertity;

import lombok.Data;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 各个模块公共的配置对象
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/23 23:56
 */
@Data
public class ModuleProperties {

    public static final String CONDITIONAL_ON_PROPERTY_VALUE_NAME = "enable";

    /**
     * 是否开启存储
     */
    private Boolean            enable                             = true;

    /**
     * controller请求前缀
     */
    private String             apiPathPrefix;

    /**
     * 判断是否启用
     * @return
     */
    public boolean isEnabled() {
        return Boolean.TRUE.equals(this.enable);
    }

    /**
     * 判断是否没启用
     * @return
     */
    public boolean notEnabled() {
        return !Boolean.TRUE.equals(this.enable);
    }
}

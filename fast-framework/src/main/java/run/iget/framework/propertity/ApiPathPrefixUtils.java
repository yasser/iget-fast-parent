package run.iget.framework.propertity;

import java.util.Objects;
import java.util.Set;

import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 为指定的Controller请求路径，添加上统一的前缀
 * ---------------类描述-----------------
 * @author 大周
 * @date 2023/1/23 23:56
 */
public class ApiPathPrefixUtils {

    /**
     * 为指定的Controller请求路径，添加上统一的前缀
     * @param properties 配置对象
     * @param configurer 路径配置对象
     * @param controllerPackages 需要添加路径前缀的包名
     */
    public static void addApiPathPrefix(ModuleProperties properties, PathMatchConfigurer configurer,
                                        String... controllerPackages) {
        if (Objects.isNull(controllerPackages) || controllerPackages.length == 0) {
            return;
        }
        for (String controllerPackage : controllerPackages) {
            addApiPathPrefix(properties, configurer, controllerPackage);
        }
    }

    /**
     * 为指定的Controller请求路径，添加上统一的前缀
     * @param properties 配置对象
     * @param configurer 路径配置对象
     * @param controllerPackages 需要添加路径前缀的包名
     */
    public static void addApiPathPrefix(ModuleProperties properties, PathMatchConfigurer configurer,
                                        Set<String> controllerPackages) {
        if (CollUtil.isEmpty(controllerPackages)) {
            return;
        }
        controllerPackages.forEach(controllerPackage -> addApiPathPrefix(properties, configurer, controllerPackage));
    }

    /**
     * 为指定的Controller请求路径，添加上统一的前缀
     * @param properties 配置对象
     * @param configurer 路径配置对象
     * @param controllerPackage 需要添加路径前缀的包名
     */
    public static void addApiPathPrefix(ModuleProperties properties, PathMatchConfigurer configurer,
                                        String controllerPackage) {
        if (ObjectUtil.hasNull(properties, configurer)
                || StrUtil.hasBlank(properties.getApiPathPrefix(), controllerPackage)) {
            return;
        }
        if (!Boolean.TRUE.equals(properties.getEnable())) {
            return;
        }
        // 为指定包下的controller添加前缀
        configurer.addPathPrefix(properties.getApiPathPrefix(),
                c -> Objects.equals(controllerPackage, c.getPackage().getName()));
    }

}

package run.iget.framework.event;

import org.springframework.context.ApplicationEvent;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 默认事件对象
 * ---------------类描述-----------------
 * @author 大周
 * @date 2023/1/17 15:05
 */
public class AppEvent extends ApplicationEvent {

    public AppEvent(Object source) {
        super(source);
    }

    public static AppEvent of(Object source) {
        return new AppEvent(source);
    }
}

package run.iget.framework.event;

import java.util.Objects;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Setter;

@Configuration
@ConfigurationProperties(prefix = CommonConst.ASYNC_MODULE_NAME)
@Setter
public class AsyncExecutorProperties {

    /**
     * 核心线程数
     */
    private Integer core;
    /**
     * 最大线程数
     */
    private Integer max;
    /**
     * 任务队列最大容量
     */
    private Integer queueCapacity;
    /**
     * 线程前缀
     */
    private String  prefix;

    public Integer getCore() {
        return Objects.nonNull(core) ? core : Runtime.getRuntime().availableProcessors();
    }

    public Integer getMax() {
        return Objects.nonNull(max) ? max : getCore() * 2;
    }

    public Integer getQueueCapacity() {
        return Objects.nonNull(queueCapacity) ? queueCapacity : getMax() * 50;
    }

    public String getPrefix() {
        return Objects.nonNull(prefix) ? prefix : "IGet-T-";
    }
}

package run.iget.framework.desensitization.enums;

import java.util.function.Function;

import cn.hutool.core.util.DesensitizedUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 脱敏类型
 * ---------------类描述-----------------
 * @author 大周
 * @since 2022/8/22 11:22
 */
@Getter
@AllArgsConstructor
public enum DesensitizationTypeEnum {

    PHONE("手机号", a -> DesensitizedUtil.mobilePhone(a)),
    ID_CARD("身份证号", a -> DesensitizedUtil.idCardNum(a, 3, 3)),
    EMAIL("邮箱号", a -> DesensitizedUtil.email(a)),
    PASSWORD("密码", a -> DesensitizedUtil.password(a)),
    CHINESE_NAME("中文名称", a -> DesensitizedUtil.chineseName(a)),
    ADDRESS("地址", a -> DesensitizedUtil.address(a, 5)),
    EMPTY("置空", new EmptyHandler());

    private String                   name;

    private Function<String, String> function;

}

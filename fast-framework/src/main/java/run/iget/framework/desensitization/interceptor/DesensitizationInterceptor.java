package run.iget.framework.desensitization.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import run.iget.framework.common.util.WebUtils;
import run.iget.framework.desensitization.context.DesensitizationThreadLocalContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DesensitizationInterceptor implements HandlerInterceptor {

    public static final String NOT_NEED_DESENSITIZATION_PARAM_NAME = "notNeedDesensitization";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String parameter = WebUtils.getParameter(NOT_NEED_DESENSITIZATION_PARAM_NAME);
        DesensitizationThreadLocalContext.put(parameter);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        DesensitizationThreadLocalContext.clear();
    }

}

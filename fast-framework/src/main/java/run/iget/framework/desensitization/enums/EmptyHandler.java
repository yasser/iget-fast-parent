package run.iget.framework.desensitization.enums;

import java.util.function.Function;

import cn.hutool.core.util.StrUtil;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 【<p>
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/22 22:17
 */
public class EmptyHandler implements Function<String, String> {

    @Override
    public String apply(String s) {
        return StrUtil.EMPTY;
    }
}

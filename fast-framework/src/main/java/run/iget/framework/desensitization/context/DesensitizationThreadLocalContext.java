package run.iget.framework.desensitization.context;

import org.springframework.stereotype.Component;

import run.iget.framework.common.enums.YesOrNoEnum;

@Component
public class DesensitizationThreadLocalContext {

    private static final ThreadLocal<String> THREAD_LOCAL_SET = new ThreadLocal<>();

    public static void put(String value) {
        THREAD_LOCAL_SET.set(value);
    }

    public static void clear() {
        THREAD_LOCAL_SET.remove();
    }

    /**
     * 判断是否需要脱敏
     * @return
     */
    public static boolean isNeedDesensitization() {
        return !YesOrNoEnum.Y.getCode().equals(THREAD_LOCAL_SET.get());
    }
}

package run.iget.framework.desensitization.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.function.Function;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import run.iget.framework.desensitization.enums.DesensitizationTypeEnum;
import run.iget.framework.desensitization.enums.EmptyHandler;
import run.iget.framework.desensitization.serializer.DesensitizeSerializer;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 数据字段脱敏注解，标记的字段，在json序列化时会进行脱敏处理
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2022/8/22 11:15
 */
@Target({ ElementType.FIELD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonSerialize(using = DesensitizeSerializer.class)
public @interface Desensitization {

    /**
     * 类型
     */
    DesensitizationTypeEnum type() default DesensitizationTypeEnum.EMPTY;

    /**
     * 脱敏处理器，默认为置空
     * 方便扩展自定义的脱敏实现
     * @return
     */
    Class<? extends Function> handler() default EmptyHandler.class;
}

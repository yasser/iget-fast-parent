package run.iget.framework.common.exception;

import lombok.Getter;
import run.iget.framework.common.enums.BaseResultEnum;

/**
 * 代码千万行，注释第一行；注释不规范，迭代两行泪
 * ----------------------------------------
 * 业务异常
 * ----------------------------------------
 * author: 大周
 * date: 2022/2/7 17:21
 **/
@Getter
public class BusinessException extends RuntimeException {

    /**
     * 异常返回枚举对象
     */
    private BaseResultEnum resultEnum;

    /**
     * 带参构造方法
     * @param msg 异常信息
     */
    public BusinessException(String msg) {
        this(BaseResultEnum.ERROR_PARAM.getCode(), msg);
    }

    /**
     * 带参构造方法
     * @param msg 异常信息
     */
    public BusinessException(String code, String msg) {
        this(BaseResultEnum.of(code, msg), null);
    }

    /**
     * 带参构造方法
     * @param resultEnum
     */
    public BusinessException(BaseResultEnum resultEnum) {
        this(resultEnum, null);
    }

    /**
     * 带参构造方法
     * @param resultEnum -- 异常返回对象
     * @param cause -- 异常根因
     */
    public BusinessException(BaseResultEnum resultEnum, Throwable cause) {
        super(resultEnum.getDesc(), cause);
        this.resultEnum = resultEnum;
    }

}

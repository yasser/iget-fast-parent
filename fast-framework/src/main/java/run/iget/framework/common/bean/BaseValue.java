package run.iget.framework.common.bean;

import lombok.Data;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 基础的对象
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/7/25 10:28
 */
@Data
public class BaseValue<T> {

    /**
     * 主键
     */
    private T id;

    /**
     * 名称
     */
    private String name;

    /**
     * 对应的值
     */
    private String value;

    /**
     * 描述
     */
    private String desc;
}

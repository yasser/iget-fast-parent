package run.iget.framework.common.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;

public class CheckUtils {

    public static boolean isBlank(String obj) {
        return StrUtil.isBlank(obj);
    }

    public static boolean isNotBlank(String obj) {
        return StrUtil.isNotBlank(obj);
    }

    public static boolean isTrue(Boolean obj) {
        return Objects.equals(Boolean.TRUE, obj);
    }

    public static boolean isAnyTrue(Boolean... obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        return Arrays.stream(obj).anyMatch(item -> isTrue(item));
    }

    public static boolean isAnyNotTrue(Boolean... obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        return Arrays.stream(obj).anyMatch(item -> isNotTrue(item));
    }

    public static boolean isAllTrue(Boolean... obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        return Arrays.stream(obj).allMatch(item -> isTrue(item));
    }

    public static boolean isNotTrue(Object obj) {
        return !Objects.equals(Boolean.TRUE, obj);
    }

    public static boolean isFalse(Boolean obj) {
        return Objects.equals(Boolean.FALSE, obj);
    }

    public static boolean isNotFalse(Object obj) {
        return !Objects.equals(Boolean.FALSE, obj);
    }

    public static boolean isNull(Object obj) {
        return Objects.isNull(obj);
    }

    public static boolean isNotNull(Object obj) {
        return Objects.nonNull(obj);
    }

    public static boolean isAnyNull(Object... obj) {
        if (Objects.isNull(obj)) {
            return true;
        }
        return Arrays.stream(obj).anyMatch(item -> isNull(item));
    }

    public static boolean isAnyNotNull(Object... obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        return Arrays.stream(obj).anyMatch(item -> isNotNull(item));
    }

    public static boolean isAllNull(Object... obj) {
        if (Objects.isNull(obj)) {
            return true;
        }
        return Arrays.stream(obj).allMatch(item -> isNull(item));
    }

    public static boolean isAllNotNull(Object... obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        return Arrays.stream(obj).allMatch(item -> isNotNull(item));
    }

    public static boolean isEmpty(Map obj) {
        return CollectionUtil.isEmpty(obj);
    }

    public static boolean isNotEmpty(Map obj) {
        return CollectionUtil.isNotEmpty(obj);
    }

    public static boolean isAnyEmpty(Map... obj) {
        if (Objects.isNull(obj)) {
            return true;
        }
        return Arrays.stream(obj).anyMatch(item -> isEmpty(item));
    }

    public static boolean isAnyNotEmpty(Map... obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        return Arrays.stream(obj).anyMatch(item -> isNotEmpty(item));
    }

    public static boolean isAllEmpty(Map... obj) {
        if (Objects.isNull(obj)) {
            return true;
        }
        return Arrays.stream(obj).allMatch(item -> isEmpty(item));
    }

    public static boolean isAllNotEmpty(Map... obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        return Arrays.stream(obj).allMatch(item -> isNotEmpty(item));
    }

    public static boolean isEmpty(Collection obj) {
        return CollectionUtil.isEmpty(obj);
    }

    public static boolean isNotEmpty(Collection obj) {
        return CollectionUtil.isNotEmpty(obj);
    }

    public static boolean isAnyEmpty(Collection... obj) {
        if (Objects.isNull(obj)) {
            return true;
        }
        return Arrays.stream(obj).anyMatch(item -> isEmpty(item));
    }

    public static boolean isAnyNotEmpty(Collection... obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        return Arrays.stream(obj).anyMatch(item -> isNotEmpty(item));
    }

    public static boolean isAllEmpty(Collection... obj) {
        if (Objects.isNull(obj)) {
            return true;
        }
        return Arrays.stream(obj).allMatch(item -> isEmpty(item));
    }

    public static boolean isAllNotEmpty(Collection... obj) {
        if (Objects.isNull(obj)) {
            return false;
        }
        return Arrays.stream(obj).allMatch(item -> isNotEmpty(item));
    }

}

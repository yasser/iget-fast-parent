package run.iget.framework.common.entity;

import lombok.Data;

@Data
public class BaseStatusEntity extends BaseEntity {

    /**
     * 逻辑删除，0启用，1禁用
     * @see run.iget.framework.common.enums.BaseStatusEnum
     */
    private Integer status;
}

package run.iget.framework.common.propertity;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
@ConfigurationProperties(prefix = "fast.framework")
public class GlobalExceptionProperties {

    /**
     * 是否输出异常信息
     */
    private Boolean             consoleError = false;

    /**
     * 异常类，对应的错误信息
     */
    private Map<String, String> exceptionClassMsg;
}

package run.iget.framework.common.bean;

import lombok.Getter;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;

@Getter
public class ExcelExportSet {

    /**
     * sheet的名称
     */
    private String sheetName;

    /**
     * 字段与别名映射
     */
    private Map<String, String> headerAlias;
    /**
     * 字段数据转换
     */
    private Map<String, Function> fieldValueFunctionMap;

    private ExcelExportSet(String sheetName) {
        this.sheetName = sheetName;
        headerAlias = new LinkedHashMap<>();
        fieldValueFunctionMap = new HashMap<>();
    }

    private ExcelExportSet() {
        headerAlias = new LinkedHashMap<>();
        fieldValueFunctionMap = new HashMap<>();
    }

    public static ExcelExportSet of(String sheetName) {
        return new ExcelExportSet(sheetName);
    }

    public static ExcelExportSet of() {
        return new ExcelExportSet();
    }

    public ExcelExportSet addHeaderAlias(String fieldName, String aliasName) {
        headerAlias.put(fieldName, aliasName);
        return this;
    }

    public ExcelExportSet addFieldValueFunction(String fieldName, Function function) {
        fieldValueFunctionMap.put(fieldName, function);
        return this;
    }

    public ExcelExportSet addFieldAliasAndValueFunction(String fieldName, String aliasName, Function function) {
        addHeaderAlias(fieldName, aliasName);
        addFieldValueFunction(fieldName, function);
        return this;
    }

    public Function getFieldValueFunction(String fieldName) {
        return this.getFieldValueFunctionMap().get(fieldName);
    }
}

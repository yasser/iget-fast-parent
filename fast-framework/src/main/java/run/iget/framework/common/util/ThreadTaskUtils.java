package run.iget.framework.common.util;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class ThreadTaskUtils {

    private static final int                   MAX_QUEUE_CAPACITY_TEN_THOUSAND = 100000;
    private static final int                   CORE_POOL_SIZE                  = 4;
    private static final int                   MAX_POOL_SIZE                   = 32;

    /**
     * 线程池
     */
    private static volatile ThreadPoolExecutor EXECUTE_POOL                    = new ThreadPoolExecutor(CORE_POOL_SIZE,
            MAX_POOL_SIZE, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>(MAX_QUEUE_CAPACITY_TEN_THOUSAND),
            new ThreadFactoryBuilder().setNameFormat("ThreadTaskUtils-pool-%d").build());

    /**
     * 提交任务到线程池中
     *
     * @param runnable
     * @throws RejectedExecutionException
     */
    public static Future<?> submitTask(Runnable runnable) throws RejectedExecutionException {
        return EXECUTE_POOL.submit(runnable);
    }

    /**
     * 提交任务到线程池中
     *
     * @param callable
     * @throws RejectedExecutionException
     */
    public static <T> Future<T> submitTask(Callable<T> callable) throws RejectedExecutionException {
        return EXECUTE_POOL.submit(callable);
    }
}

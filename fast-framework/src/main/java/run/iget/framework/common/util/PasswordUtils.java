package run.iget.framework.common.util;

import cn.hutool.crypto.SecureUtil;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 密码工具栏
 * ---------------类描述-----------------
 * @author 大周
 * @date 2023/1/17 14:11
 */
public class PasswordUtils {

    public static String md5(String text) {
        return md5(text, "");
    }

    public static String md5(String text, String salt) {
        return md5(text, salt, 0);
    }

    public static String md5(String text, String salt, int cycleNum) {
        String result = SecureUtil.md5(text + salt);
        for (int i = 0; i < cycleNum; i++) {
            result = SecureUtil.md5(result + salt);
        }
        return result;
    }

    public static boolean matches(String password, String text, String salt) {
        return md5(text, salt).equals(password);
    }

    public static boolean matches(String password, String text) {
        return md5(text).equals(password);
    }
}

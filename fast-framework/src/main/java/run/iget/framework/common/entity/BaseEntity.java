package run.iget.framework.common.entity;

import java.util.Date;

import lombok.Data;

@Data
public class BaseEntity extends CreateTimeRemarkEntity {

    /**
     * 更新时间
     */
    private Date updateTime;
}

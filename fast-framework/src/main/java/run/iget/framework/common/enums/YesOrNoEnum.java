package run.iget.framework.common.enums;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 代码千万行，注释第一行；注释不规范，迭代两行泪
 * ----------------------------------------
 * yes or no
 * ----------------------------------------
 * author: 大周
 * date: 2022/2/8 10:54
 **/
@Getter
@AllArgsConstructor
public enum YesOrNoEnum implements BaseEnum<String> {

    Y("Y", "yes"),
    N("N", "no");

    /**
     * 返回码
     */
    private String code;

    /**
     * 返回消息
     */
    private String desc;

    /**
     * 取反
     * @param yesOrNoEnum 入参
     * @return 如果yesOrNoEnum == null，则返回null。
     */
    public static YesOrNoEnum negate(YesOrNoEnum yesOrNoEnum) {
        if (yesOrNoEnum == null) {
            return null;
        }
        return negate(yesOrNoEnum.code);
    }

    /**
     * 取反
     * @param code 入参
     * @return 如果yesOrNoEnum == null，则返回null。
     */
    public static YesOrNoEnum negate(String code) {
        if (StrUtil.isBlank(code)) {
            return null;
        }
        return Y.code.equals(code) ? N : Y;
    }

}

package run.iget.framework.common.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

import lombok.extern.slf4j.Slf4j;
import run.iget.framework.event.AppEvent;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * Spring上下文对象获取工具类
 * ---------------类描述-----------------
 * @author 大周
 * @since 2021/7/15 23:12:07
 */
@Slf4j
@Component
public final class SpringContextHolder implements ApplicationContextAware {

    /**
     * spring的上下文对象
     */
    private static ApplicationContext APPLICATION_CONTEXT;

    /**
     * 功能描述: 通过类类型获取bean
     * @param tClass -- <描述>
     * @return
     */
    public static <T> T getBean(Class<T> tClass) {
        return APPLICATION_CONTEXT.getBean(tClass);
    }

    /**
     * 功能描述: 通过类类型获取bean
     * @param tClass -- <描述>
     * @param params -- 构造参数
     * @return
     */
    public static <T> T getBean(Class<T> tClass, Object... params) {
        return APPLICATION_CONTEXT.getBean(tClass, params);
    }

    /**
     * 功能描述: 通过类名称
     * @param beanName
     * @return
     */
    public static Object getBean(String beanName) {
        return APPLICATION_CONTEXT.getBean(beanName);
    }

    /**
     * 功能描述: 通过类名称
     * @param beanName -- 类名称
     * @param params -- 构造参数
     * @return
     */
    public static Object getBean(String beanName, Object... params) {
        return APPLICATION_CONTEXT.getBean(beanName, params);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        APPLICATION_CONTEXT = applicationContext;
    }

    /**
     * 发送事件
     * @param event
     */
    public static void publish(AppEvent event) {
        if (log.isDebugEnabled()) {
            log.debug("发布事件消息：{}", JSON.toJSONString(event));
        }
        APPLICATION_CONTEXT.publishEvent(event);
    }
}

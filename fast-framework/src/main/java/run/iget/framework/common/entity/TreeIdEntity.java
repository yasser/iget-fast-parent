package run.iget.framework.common.entity;

import java.util.List;

import com.mybatisflex.annotation.Column;

import lombok.Data;

/**
 * Entity基类
 *
 * @author 阿沐 babamu@126.com
 */
@Data
public class TreeIdEntity extends IdEntity {

    /**
     * 父节点
     */
    private Long pid;

    /**
     * 子节点
     */
    @Column(ignore = true)
    private List children;

}

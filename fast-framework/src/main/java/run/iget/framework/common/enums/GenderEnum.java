/*
 * Copyright 2021 Zhongan.io All right reserved. This software is the
 * confidential and proprietary information of Zhongan.io ("Confidential
 * Information"). You shall not disclose such Confidential Information and shall
 * use it only in accordance with the terms of the license agreement you entered
 * into with Zhongan.io.
 */
package run.iget.framework.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 类GenderEnum的实现描述：性别枚举
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/8/23 20:36
 */
@Getter
@AllArgsConstructor
public enum GenderEnum implements BaseEnum<String> {
    /**
     * 男性
     */
    MALE("1", "男性"),
    /**
     * 女性
     */
    FEMALE("2", "女性"),
    /**
     * 未知
     */
    UNKNOWN("3", "未知");

    /**
     * 代码
     */
    private String code;

    /**
     * 描述
     */
    private String desc;
}

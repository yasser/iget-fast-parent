package run.iget.framework.common.enums;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 公共模块枚举对象
 * ---------------类描述-----------------
 * @author 大周
 * @date 2023/1/17 17:32
 */
public interface BaseResultEnum extends BaseEnum<String> {

    BaseResultEnum SUCCESS         = BaseResultEnum.of("0", "处理成功");
    BaseResultEnum ERROR_PARAM     = BaseResultEnum.of("1", "参数错误");
    BaseResultEnum ERROR_LOGIN     = BaseResultEnum.of("2", "还未授权，不能访问");
    BaseResultEnum ERROR_AUTH      = BaseResultEnum.of("3", "没有权限，禁止访问");
    BaseResultEnum ERROR_404       = BaseResultEnum.of("4", "请求资源不存在");
    BaseResultEnum ERROR_500       = BaseResultEnum.of("5", "服务器异常，请稍后再试");
    BaseResultEnum ERROR_CAPTCHA   = BaseResultEnum.of("6", "验证码错误");
    BaseResultEnum ERROR_AUTH_SAFE = BaseResultEnum.of("7", "没有二次认证，禁止访问");

    static BaseResultEnum of(String code, String desc) {
        return new BaseResultEnum() {
            @Override
            public String getCode() {
                return code;
            }

            @Override
            public String getDesc() {
                return desc;
            }
        };
    }

    /**
     * 系统模块
     * @return base
     */
    default String getModule() {
        return "base";
    }

    public static void main(String[] args) {
    }
}

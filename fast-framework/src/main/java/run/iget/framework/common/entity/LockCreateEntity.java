package run.iget.framework.common.entity;

import lombok.Data;
import run.iget.framework.common.enums.YesOrNoEnum;

@Data
public class LockCreateEntity {

    /**
     * 版本号
     */
    private Integer version;

    /**
     * 删除标记
     * Y 是，N 否
     *
     * @see YesOrNoEnum
     */
    private String  deleted;

}

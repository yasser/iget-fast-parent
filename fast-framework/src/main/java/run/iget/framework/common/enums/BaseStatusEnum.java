package run.iget.framework.common.enums;

import java.util.Objects;

import lombok.Getter;

/**
 * 代码千万行，注释第一行；注释不规范，迭代两行泪
 * ----------------------------------------
 * 数据库逻辑删除枚举对象
 * ----------------------------------------
 * author: 大周
 * date: 2022/2/8 10:59
 **/
@Getter
public enum BaseStatusEnum implements BaseEnum<Integer> {
    /**
     * 0启用，禁用
     */
    ENABLE(0, "启用"),
    DISABLE(1, "禁用");

    /**
     * 关键值
     */
    private Integer code;
    /**
     * 描述
     */
    private String  desc;

    private BaseStatusEnum(int value, String description) {
        this.code = value;
        this.desc = description;
    }

    public static boolean isDisable(Integer value) {
        return Objects.equals(value, BaseStatusEnum.DISABLE.code);
    }

    /**
     * 取反
     * @param statusEnum 入参
     * @return 如果statusEnum == null，则返回null。
     */
    public static BaseStatusEnum negate(BaseStatusEnum statusEnum) {
        if (statusEnum == null) {
            return null;
        }
        return negate(statusEnum.code);
    }

    /**
     * 取反
     * @param statusCode 入参
     * @return 如果statusEnum == null，则返回null。
     */
    public static BaseStatusEnum negate(Integer statusCode) {
        if (statusCode == null) {
            return null;
        }
        return isDisable(statusCode) ? BaseStatusEnum.ENABLE : BaseStatusEnum.DISABLE;
    }

}

package run.iget.framework.common.resp;

import lombok.Data;
import run.iget.framework.common.enums.BaseResultEnum;

/**
 * 响应数据
 *
 * @author 阿沐 babamu@126.com
 */
@Data
public class ResultResp<T> {

    private String code;

    private String msg;

    private String module;

    private T      data;

    public static <T> ResultResp<T> ok() {
        return ok(null);
    }

    public static <T> ResultResp<T> ok(T data) {
        return of(BaseResultEnum.SUCCESS, data);
    }

    public static <T> ResultResp<T> error() {
        return error(BaseResultEnum.ERROR_500);
    }

    public static <T> ResultResp<T> error(String msg) {
        return error(BaseResultEnum.ERROR_500.getCode(), msg);
    }

    public static <T> ResultResp<T> error(BaseResultEnum errorCode) {
        return error(errorCode.getCode(), errorCode.getDesc());
    }

    public static <T> ResultResp<T> error(String code, String msg) {
        ResultResp<T> result = new ResultResp<>();
        result.setCode(code);
        result.setMsg(msg);
        result.setModule(BaseResultEnum.ERROR_500.getModule());
        return result;
    }

    public static <T> ResultResp<T> of(BaseResultEnum moduleEnum, T data) {
        ResultResp<T> result = new ResultResp<>();
        result.setCode(moduleEnum.getCode());
        result.setMsg(moduleEnum.getDesc());
        result.setModule(moduleEnum.getModule());
        result.setData(data);
        return result;
    }
}

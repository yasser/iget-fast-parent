package run.iget.framework.common.entity;

import lombok.Data;

/**
 * Entity基类
 *
 * @author 阿沐 babamu@126.com
 */
@Data
public abstract class CreatorEntity extends CreateTimeEntity {

    /**
     * 创建者
     */
    private String creator;
}

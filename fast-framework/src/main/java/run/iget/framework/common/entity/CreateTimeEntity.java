package run.iget.framework.common.entity;

import java.util.Date;

import lombok.Data;

@Data
public class CreateTimeEntity extends IdEntity {

    /**
     * 创建时间
     */
    private Date createTime;

}

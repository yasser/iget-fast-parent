package run.iget.framework.common.entity;

import java.util.Date;

import lombok.Data;

@Data
public class CreateTimeRemarkEntity extends IdEntity {

    /**
     * 创建时间
     */
    private Date   createTime;

    /**
     * 备注信息
     */
    private String remark;
}

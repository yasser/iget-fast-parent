package run.iget.framework.common.deserializer;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;
import run.iget.framework.common.util.DateUtils;
import run.iget.framework.common.util.ExceptionThrowUtils;

import java.io.IOException;
import java.time.LocalDate;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 将入参中的字符串解析为Date
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/8/16 14:37
 */
@Slf4j
public class StringToLocalDateDeserializer extends JsonDeserializer<LocalDate> {

    @Override
    public LocalDate deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException, JacksonException {
        try {
            return DateUtils.tryParseLocalDate(jsonParser.getText());
        } catch (Exception e) {
            log.debug("入参日期解析错误", e);
            ExceptionThrowUtils.of("日期格式错误");
        }
        return null;
    }
}

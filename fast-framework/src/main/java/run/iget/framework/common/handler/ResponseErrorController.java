package run.iget.framework.common.handler;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import run.iget.framework.common.enums.BaseResultEnum;
import run.iget.framework.common.util.ExceptionThrowUtils;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 错误路径返回json对象
 * ---------------类描述-----------------
 *
 * @author 大周
 * @since 2021/7/15 23:14:00
 */
@Controller
public class ResponseErrorController implements ErrorController {

    /**
     * 默认出错的请求路径
     */
    private static final String ERROR_PATH = "/error";

    /**
     * 请求错误时访问的地址
     */
    @RequestMapping(ERROR_PATH)
    public void handleError(HttpServletRequest request) throws Exception {
        Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
        ExceptionThrowUtils.ofTrue(Objects.equals(statusCode, 400), BaseResultEnum.ERROR_LOGIN);
        ExceptionThrowUtils.of(BaseResultEnum.ERROR_404);
    }
}

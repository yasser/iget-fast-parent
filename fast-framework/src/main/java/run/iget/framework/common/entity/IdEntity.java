package run.iget.framework.common.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.KeyType;
import lombok.Data;

import java.io.Serializable;

/**
 * Entity基类
 *
 * @author 阿沐 babamu@126.com
 */
@Data
public class IdEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 自增主键id
     */
    @Id(keyType = KeyType.Auto)
    private Long id;

}

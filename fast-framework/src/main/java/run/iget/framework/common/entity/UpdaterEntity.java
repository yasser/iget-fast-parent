package run.iget.framework.common.entity;

import lombok.Data;

@Data
public class UpdaterEntity extends CreatorEntity {

    /**
     * 修改者
     */
    private String creator;
}

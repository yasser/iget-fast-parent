package run.iget.framework.common.enums;

import java.util.Arrays;
import java.util.Objects;

/**
 * 代码千万行，注释第一行；注释不规范，迭代两行泪
 * ----------------------------------------
 * 集成的枚举类
 * ----------------------------------------
 * author: 大周
 * date: 2022/2/7 17:26
 **/
public interface BaseEnum<T> {

    /**
     * key
     *
     * @return
     */
    T getCode();

    /**
     * 描述
     *
     * @return
     */
    String getDesc();

    static <T, E extends Enum<E> & BaseEnum<T>> E getByCode(Class<E> enumClass, T code) {
        E[] enums = enumClass.getEnumConstants();
        return Arrays.stream(enums).filter(v -> Objects.equals(code, v.getCode())).findAny().orElse(null);
    }

    static <T, E extends Enum<E> & BaseEnum<T>> E getByDesc(Class<E> enumClass, String desc) {
        E[] enums = enumClass.getEnumConstants();
        return Arrays.stream(enums).filter(v -> Objects.equals(desc, v.getDesc())).findAny().orElse(null);
    }

    static <T, E extends Enum<E> & BaseEnum<T>> String getDescByCode(Class<E> enumClass, T code) {
        E byCode = getByCode(enumClass, code);
        if (Objects.nonNull(byCode)) {
            return byCode.getDesc();
        }
        return null;
    }

}

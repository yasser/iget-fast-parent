package run.iget.framework.common.util;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 统一的应用启动器
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/2/17 14:55
 */
@Slf4j
public class SpringBootLauncher {

    /**
     * 启动
     *
     * @param mainClass
     * @param args
     * @return
     */
    public static ConfigurableApplicationContext run(Class mainClass, String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(mainClass, args);
        Environment env = applicationContext.getEnvironment();
        String applicationName = env.getProperty("spring.application.name");
        if (StrUtil.isBlank(applicationName)) {
            applicationName = mainClass.getSimpleName();
        }
        log.info("----------------------------------------------------------");
        log.info("Application '{}', at port {} is running!", applicationName, env.getProperty("server.port"));
        log.info("----------------------------------------------------------");

        return applicationContext;
    }

}

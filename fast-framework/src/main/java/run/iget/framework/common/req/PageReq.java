package run.iget.framework.common.req;

import com.mybatisflex.core.paginate.Page;
import lombok.Data;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 分页请求对象
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/7/18 11:23
 */
@Data
public class PageReq<T> {

    /**
     * 当前页码。
     */
    private Integer pageNumber = 1;

    /**
     * 每页数据数量。
     */
    private Integer pageSize = 10;

    /**
     * 排序字段
     */
    String order;

    /**
     * 是否升序
     */
    Boolean asc;

    /**
     * 数据
     */
    T queryBean;

    public Page getPage() {
        return Page.of(pageNumber, pageSize);
    }
}

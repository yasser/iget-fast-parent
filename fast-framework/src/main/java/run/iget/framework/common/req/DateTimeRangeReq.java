package run.iget.framework.common.req;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.util.List;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 日期范围查询
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/7/18 11:32
 */
@Data
public class DateTimeRangeReq {

    /**
     * 字段名称
     */
    private String column;

    /**
     * 开始
     */
    private String from;

    /**
     * 结束
     */
    private String to;

    /**
     * 开始、结束形成数组
     */
    private List<String> timeRange;

    public String getStartTime() {
        if (StrUtil.isNotBlank(from)) {
            return from;
        }
        if (CollectionUtil.isNotEmpty(timeRange)) {
            return timeRange.get(0);
        }
        return null;
    }

    public String getEndTime() {
        if (StrUtil.isNotBlank(to)) {
            return to;
        }
        if (CollectionUtil.isNotEmpty(timeRange) && timeRange.size() > 1) {
            return timeRange.get(1);
        }
        return null;
    }
}

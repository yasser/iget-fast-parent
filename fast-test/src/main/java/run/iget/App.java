package run.iget;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

import run.iget.framework.common.util.SpringBootLauncher;

@EnableAsync
@SpringBootApplication
@MapperScan("run.iget.**.mapper")
public class App {
    public static void main(String[] args) {
        SpringBootLauncher.run(App.class, args);
    }
}

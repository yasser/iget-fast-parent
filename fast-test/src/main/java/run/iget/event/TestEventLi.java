package run.iget.event;

import java.util.Objects;

import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TestEventLi implements ApplicationListener<TestEvent> {

    @Async
    @Override
    public void onApplicationEvent(TestEvent event) {
        log.info("我已经监听到");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if (Objects.isNull(event) || Objects.isNull(event.getSource())) {
            return;
        }
        log.info("异步任务执行---------1秒");
    }
}

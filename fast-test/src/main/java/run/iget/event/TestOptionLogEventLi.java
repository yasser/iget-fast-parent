package run.iget.event;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import run.iget.security.event.SafeAuthOptionLogEvent;

@Slf4j
@Component
public class TestOptionLogEventLi implements ApplicationListener<SafeAuthOptionLogEvent> {

    @Async
    @Override
    public void onApplicationEvent(SafeAuthOptionLogEvent event) {
        log.info("我已经监听到: {}", JSON.toJSONString(event));
    }
}

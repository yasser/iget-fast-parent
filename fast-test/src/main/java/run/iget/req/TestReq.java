package run.iget.req;

import cn.hutool.core.date.DatePattern;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class TestReq implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    private Date date;

    @JsonFormat(pattern = DatePattern.NORM_DATE_PATTERN)
    private Date date1;

    private Date date2;
}

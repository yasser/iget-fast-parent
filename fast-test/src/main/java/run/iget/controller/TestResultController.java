package run.iget.controller;

import cn.hutool.core.util.IdUtil;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import run.iget.req.TestReq;

import java.util.Date;

@RestController
public class TestResultController {


    @RequestMapping("/result")
    public String test() {
        return IdUtil.fastSimpleUUID();
    }

    @RequestMapping("/req")
    public Object test(@RequestBody TestReq req) {
        req.setDate1(new Date());
        req.setDate2(new Date());
        return req;
    }


}

package run.iget.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.core.util.RandomUtil;
import lombok.extern.slf4j.Slf4j;
import run.iget.event.TestEvent;
import run.iget.framework.common.context.SpringContextHolder;
import run.iget.framework.event.EventPublishUtils;
import run.iget.security.bean.SafeAuthUser;
import run.iget.security.event.AuthEventEnums;
import run.iget.security.event.SafeAuthEvent;

@Slf4j
@RestController
public class TestEventController {

    @PostMapping("/push")
    public SafeAuthUser push() {
        long l = RandomUtil.randomLong();
        SafeAuthUser safeAuthUser = new SafeAuthUser();
        safeAuthUser.setId(l);
        SafeAuthEvent safeAuthEvent = new SafeAuthEvent(safeAuthUser, AuthEventEnums.LOGIN);
        EventPublishUtils.publish(safeAuthEvent);
        return safeAuthUser;
    }

    @PostMapping("/push2")
    public SafeAuthUser push2() {
        long l = RandomUtil.randomLong();
        SafeAuthUser safeAuthUser = new SafeAuthUser();
        safeAuthUser.setId(l);
        SafeAuthEvent safeAuthEvent = new SafeAuthEvent(safeAuthUser, AuthEventEnums.LOGIN);
        SpringContextHolder.publish(safeAuthEvent);
        return safeAuthUser;
    }

    @PostMapping("/push3")
    public Long push3() {
        Long l = RandomUtil.randomLong();
        TestEvent event = new TestEvent(l);
        SpringContextHolder.publish(event);
        log.info("发布TestEvent");
        return l;
    }

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @PostMapping("/push4")
    public Long push444() {
        Long l = RandomUtil.randomLong();
        TestEvent event = new TestEvent(l);
        for (int i = 0; i < 5; i++) {
            applicationEventPublisher.publishEvent(event);
        }
        //        SpringContextHolder.publish(event);
        log.info("发布TestEvent---------------");
        return l;
    }
}

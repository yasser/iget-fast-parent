package run.iget.webcoket.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;
import run.iget.framework.propertity.ModuleProperties;
import run.iget.webcoket.constant.WSConst;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * WebSocket配置信息
 * ---------------类描述-----------------
 * @author 大周
 * @date 2023/1/24 20:54
 */
@Data
@Component
@ConfigurationProperties(prefix = WSConst.MODULE_NAME)
public class WebSocketProperties extends ModuleProperties {

    /**
     * 是否启用
     */
    private Boolean enable;

    /**
     * 端口
     */
    private Integer port;

    /**
     * 心跳超时时间(单位: 毫秒，默认12000)，如果用户不希望框架层面做心跳相关工作，请把此值设为0或负数
     */
    private Long    heartbeatTimeout;

    /**
     * 是否展示接收到的消息
     */
    private Boolean showMsg;

    /**
     * 链接时候，传递的用户标识参数名称
     * 如果为空，则为 id
     */
    private String  userSign;

}

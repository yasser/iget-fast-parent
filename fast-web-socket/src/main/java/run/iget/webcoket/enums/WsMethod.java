package run.iget.webcoket.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum WsMethod {

    ON_CONNECT("onAfterHandshaked", "握手成功后触发该方法"),
    ON_TEXT("onClose", "当收到Opcode.TEXT消息时，执行该方法。"),
    ON_CLOSE("onClose", "当收到Opcode.CLOSE时，执行该方法");

    String code;
    String desc;

}

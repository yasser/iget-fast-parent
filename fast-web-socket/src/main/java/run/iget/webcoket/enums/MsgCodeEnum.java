package run.iget.webcoket.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import run.iget.framework.common.enums.BaseEnum;

@Getter
@AllArgsConstructor
public class MsgCodeEnum implements BaseEnum<String> {

    private String code;
    private String desc;

}

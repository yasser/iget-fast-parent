package run.iget.webcoket.constant;

import java.util.Objects;

import org.tio.server.ServerTioConfig;

public class WSConst {

    public final static String    MODULE_NAME             = "fast.ws";
    public final static String    CONTROLLER_PACKAGE_NAME = "run.iget.framework.tio";

    /**
     * 连接请求中携带的用户id参数名称
     */
    public static String          CONNECT_REQ_USER_ID     = "id";

    /**
     * 连接请求中携带的其他参数名称
     */
    public static String          CONNECT_REQ_PARAM       = "param";

    /**
     * WS配置
     */
    public static ServerTioConfig TIO_SERVER_CONFIG       = null;

    /**
     * 是否打印接收的消息
     */
    public static boolean         SHOW_MSG                = false;

    /**
     * 判断是否启用
     * @return
     */
    public static boolean isEnable() {
        return Objects.nonNull(WSConst.TIO_SERVER_CONFIG);
    }

    /**
     * 判断是否禁用
     * @return
     */
    public static boolean isDisEnable() {
        return Objects.isNull(WSConst.TIO_SERVER_CONFIG);
    }
}

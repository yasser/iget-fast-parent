package run.iget.webcoket;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tio.core.maintain.IpStats;

import run.iget.webcoket.bean.WSClientMsg;
import run.iget.webcoket.constant.WSConst;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * ws发送消息控制器
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/16 19:47
 */
@RestController
@RequestMapping("/ws")
public class WSController {

    /**
     * 发送消息
     *
     * @param clientMsg
     */
    @PostMapping("/send")
    public void send(@RequestBody WSClientMsg clientMsg) {
        WSUtils.send(clientMsg.getUserId(), clientMsg.getMsg());
    }

    /**
     * 关闭客户端
     *
     * @param clientMsg
     */
    @PostMapping("/close")
    public void close(@RequestBody WSClientMsg clientMsg) {
        WSUtils.close(clientMsg.getUserId(), clientMsg.getMsg());
    }

    @PostMapping("/statistics")
    public IpStats statistics() {
        if (WSConst.isDisEnable()) {
            return null;
        }
        return WSConst.TIO_SERVER_CONFIG.ipStats;
    }

}

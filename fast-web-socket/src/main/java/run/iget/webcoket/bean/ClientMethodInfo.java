package run.iget.webcoket.bean;

import java.util.Objects;

import org.tio.core.ChannelContext;

import lombok.Data;
import run.iget.webcoket.enums.WsMethod;

@Data
public class ClientMethodInfo {

    /**
     * 客户ip地址
     */
    private String   clientIp;

    /**
     * 服务端ip
     */
    private String   serviceIp;

    /**
     * 事件来源的方法
     */
    private WsMethod method;

    /**
     * 系统为连接的客户端生成的id
     */
    private String   clientId;

    /**
     * 客户登录时候绑定的id
     */
    private String   userId;

    public ClientMethodInfo() {
    }

    public ClientMethodInfo(ChannelContext channelContext, WsMethod method) {
        this.method = method;
        //        this.channelContext = channelContext;
        if (Objects.nonNull(channelContext)) {
            this.clientId = channelContext.getId();
            this.userId = channelContext.userid;
            this.clientIp = channelContext.getClientNode().getIp();
            this.serviceIp = channelContext.getServerNode().getIp();
        }
    }
}

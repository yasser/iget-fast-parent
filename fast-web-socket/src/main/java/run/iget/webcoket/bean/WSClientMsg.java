package run.iget.webcoket.bean;

import org.tio.core.ChannelContext;

import lombok.Data;
import run.iget.webcoket.enums.WsMethod;

@Data
public class WSClientMsg extends ClientMethodInfo {

    private String code;

    /**
     * 客户发送来的消息
     */
    private String msg;

    public WSClientMsg() {
    }

    public WSClientMsg(ChannelContext channelContext, WsMethod method, String msg) {
        super(channelContext, method);
        this.msg = msg;
    }
}

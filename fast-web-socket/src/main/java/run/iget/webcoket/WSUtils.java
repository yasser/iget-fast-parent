package run.iget.webcoket;

import org.tio.core.Tio;
import org.tio.websocket.common.WsResponse;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import run.iget.webcoket.constant.WSConst;

/**
 * 代码千万行，注释第一行，注释不规范，迭代两行泪
 * ---------------类描述-----------------
 * 发现消息给客户端的工具栏
 * ---------------类描述-----------------
 *
 * @author 大周
 * @date 2023/1/16 19:40
 */
public final class WSUtils {

    /**
     * 发送给所有人
     *
     * @param msg 消息信息，如果为空，则不执行
     */
    public static void send(String msg) {
        send(null, msg);
    }

    /**
     * 发送消息
     * 如果 clientId和msg都为空，则不会执行
     *
     * @param clientId 客户端登录后绑定的用户id，如果为空，则发送给所有人
     * @param msg      消息信息
     */
    public static void send(String clientId, String msg) {
        if (WSConst.isDisEnable()) {
            return;
        }
        if (StrUtil.isAllBlank(clientId, msg)) {
            return;
        }
        WsResponse wsResponse = WsResponse.fromText(msg, CharsetUtil.UTF_8);
        if (StrUtil.isBlank(clientId)) {
            Tio.sendToAll(WSConst.TIO_SERVER_CONFIG, wsResponse);
        } else {
            Tio.sendToUser(WSConst.TIO_SERVER_CONFIG, clientId, wsResponse);
        }
    }

    /**
     * 关闭客户端连接
     *
     * @param clientId 客户端登录后绑定的用户id
     */
    public static void close(String clientId) {
        close(clientId, null);
    }

    /**
     * 关闭客户端连接，并发送消息
     *
     * @param clientId 客户端登录后绑定的用户id
     * @param msg      发送消息
     */
    public static void close(String clientId, String msg) {
        if (WSConst.isDisEnable()) {
            return;
        }
        if (StrUtil.isNotBlank(msg)) {
            send(clientId, msg);
        }
        Tio.closeUser(WSConst.TIO_SERVER_CONFIG, clientId, msg);
    }

}
